.. GEM Online Software Developer's Guide documentation master file, created by
   sphinx-quickstart on Wed Mar  4 09:39:32 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
CMS GEM online software developer documentation
===============================================

.. toctree::
  :caption: Contents
  :maxdepth: 4

  Overview <overview>
  Getting started <starting>
  codingstyle
  FAQ <faq>
  Contact <contact>


.. toctree::
   :hidden:
   :caption: Links to other documentation
   :maxdepth: 1

   CMS GEM DAQ project page <http://0.0.0.0:8000>
   User guide <http://0.0.0.0:8000/SITE_ROOT/guides/userguide>
   Expert guide <http://0.0.0.0:8000/SITE_ROOT/guides/expertguide>
   Developer guide (this site) <http://0.0.0.0:8000/SITE_ROOT/guides/devguide>
..
   https://github.com/sphinx-doc/sphinx/issues/1836
..
   :any:`userguide:index`
   :any:`expertguide:index`
   :any:`Developer guide (this site) <index>`

.. toctree::
   :hidden:
   :caption: Links to API documentation
   :maxdepth: 1

   cmsgemos <http://0.0.0.0:8000/SITE_ROOT/docs/api/cmsgemos/latest>
   gemplotting <http://0.0.0.0:8000/SITE_ROOT/docs/api/gemplotting/latest>
   vfatqc <http://0.0.0.0:8000/SITE_ROOT/docs/api/vfatqc/latest>
   ctp7_modules <http://0.0.0.0:8000/SITE_ROOT/docs/api/ctp7_modules/latest>
   reg_utils <http://0.0.0.0:8000/SITE_ROOT/docs/api/reg_utils/latest>
   xhal <http://0.0.0.0:8000/SITE_ROOT/docs/api/xhal/latest>


Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`
