.. _gemos-operations-conf-det:

======================
Configuring a detector
======================

.. contents:: Configuration methods
   :local:
   :depth: 1
   :backlinks: entry


.. _gemos-test-connectivity:

Using ``testConnectivity.py`` to configure a detector (recommended)
-------------------------------------------------------------------

The ``testConnectivity.py`` tool facilitates the operation of several standard procedures:

* :ref:`Establish communication with the frontend electronics for one or more detectors<gemos-communication-check>`.
* :ref:`Automatically scan all VFAT3 DACs<gemos-auto-dac-scan>`
* Automatically launch an scurve and analyze the data.


.. _gemos-communication-check:

Routine to establish communication w/detectors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Assuming your back-end electronics are setup correctly you can configure the front-end electronics by executing the following steps:

#. Power the LV of your detector(s),
#. Determine the ``shelf`` number of your μTCA crate,
#. Determine the ``SLOT`` of your AMC in the μTCA crate with the shelf number from step 2,
#. Determine the ``ohMask`` of your detector(s) on your AMC in slot ``SLOT``,

   .. note::
      Here ``ohMask`` is a 12-bit number, where a 1 in the :math:`N^{th}` bit means "consider this OptoHybrid."
      So an ``ohMask = 0xc4c`` would mean to use OptoHybrids 2, 3, 6, 10 and 11.

#. The execute:

.. code-block:: bash

   testConnectivity.py --skipDACScan --skipScurve --nPhaseScans=100 SHELF SLOT OHMASK 2>&1 | tee connectivityLog.log

For each OptoHybrid in ``ohMask`` this will:

#. Check GBT communication & program GBTs
#. Check SCA communication & reset SCAs
#. Program FPGA & Check Communication

   .. warning::
      This will issue a TTC hard reset to all OptoHybrids connected to the AMC in question.
      This will *wipe* out the frontend configuration and kill any running scan and stop any data-taking process.

#. Scan GBT phases & set each VFAT to a good phase
#. Synchronize all VFATs and check VFAT communication

.. tip::

   If you would like to start at a later step use the option ``-f X`` where ``X`` is the desired starting step, e.g., ``-f 3`` will start by programming the FPGA and checking FPGA communication (in this case it would be assumed that steps 1 & 2 have been completed by an earlier call of ``testConnectivity.py`` or by manual intervention).

   * You can add the option ``-i`` to ignore VFAT synchronization errors.
   * You can add the option ``-a`` to accept a bad trigger link status (e.g., trigger link fibers are not connected).

   For complete usage see the :py:mod:`documentation<vfatqc:testConnectivity>`

You are now ready to issue a configure command.
The configure command is done with ``confChamber.py``:

.. code-block:: bash

   confChamber.py cardName -gX


.. _gemos-auto-dac-scan:

Automatic DAC scan, analysis & upload of parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This procedure will scan the DACs of all VFATs.
The DAC is involved in the operation of the analog portion of the front-end.
This procedure will also automatically analyze this data to determine the correct DAC settings needed to determine the proper bias current and voltages for each DAC.

First upload the correct ``CFG_IREF`` values to the VFAT3 configuration files on the CTP7 in slot ``SLOT`` and prepare the ``ADC0`` calibration file under ``${DATA_PATH}/${DETECTOR_SER_NO}/calFile_ADC0_{DETECTOR_SER_NO}.txt`` for each detector defined in the :ref:`chamber_config<gemos-usage-chamber-info>` dictionary.

.. note::
   If the VFAT3s you are using have their chipID encoded with the `Reed-Muller Encoding Algorithm <https://en.wikipedia.org/wiki/Reed%E2%80%93Muller_code>`_ and they are found in the VFAT3 production DB then you do not need to upload the ``CFG_IREF`` values yourself or prepare the ``calFile_ADC0_{DETECTOR_SER_NO}.txt`` file as this will be done for you.

Next execute either:

.. code-block:: bash

   testConnectivity.py --skipScurve SHELF SLOT OHMASK 2>&1 | tee connectivityLog.log

or 

.. code-block:: bash

   testConnectivity.py -f5 --skipScurve SHELF SLOT OHMASK 2>&1 | tee connectivityLog.log

This will automatically perform a DAC scan, analyze the DAC scan results, and then upload the register values to the VFAT3 configuration files on the CTP7 in slot ``SLOT``.

You are now ready to issue a configure command.
The configure command is done with ``confChamber.py``:

.. code-block:: bash

   confChamber.py cardName -gX


.. _gemos-manual-conf-det:

Manually configuring a detector
-------------------------------

Assuming your back-end electronics are setup correctly you can configure the front-end electronics by executing the following steps:


#. Power the LV of your detector(s),
#. If the GBTx chips of your OptoHybrid(s) are not fully fused, program the GBTx chips, see :ref:`expertguide:gemos-gbt-programming`,
#. Program the FPGA of the OptoHybrid(s), see :ref:`gemos-optohybrid-programming`,
#. Issue a GBTx link reset, see :ref:`gemos-gbt-link-reset`,
#. Check the VFATs are all synchronized on your OptoHybrid(s), see :ref:`gemos-frontend-vfatsync`,

You are now ready to issue a configure command.
The configure command is done with ``confChamber.py``:

.. code-block:: bash

   confChamber.py cardName -gX

This will issue an RPC call to the CTP7 whose network alias is ``cardName`` and load all the per VFAT3 configuration settings in each of the VFAT3 configuration files (see :ref:`gemos-frontend-vfatcfgfile`) for OptoHybrid ``X``.
Note it is important to have edited each of these files to ensure the ``CFG_IREF`` value for your VFATs on all your OptoHybrid(s) is the unique value each chip needs.


.. _gemos-chamber-vfat-dac-settings:

Using ``chamber_vfatDACSettings`` to write common register values
-----------------------------------------------------------------

While some registers must be set by hand or by the ``replace_parameter.sh`` script described in :ref:`gemos-frontend-vfatcfgfile`, since they are unique to each VFAT (e.g., ``CFG_IREF`` or registers that control a VFAT3's analog chain), some registers can be safely applied to all VFATs (e.g., setting the comparator mode, see :ref:`gemos-frontend-vfat3-overview`).
To do this easily, and without having to tediously modify many text files on the CTP7 the ``chamber_vfatDACSettings`` dictionary exists for this purpose.

.. hint::
   The ``chamber_vfatDACSettings`` is found in the ``system_specific_constants.py`` :ref:`file<gemos-usage-chamber-info>`, and the location of this file must be in the ``$PYTHONPATH`` of your system.

The ``chamber_vfatDACSettings`` dictionary is a nested dictionary where the outer key is the geographic address (e.g., ``ohKey``), a tuple of the form ``(shelf,slot,link)``, which specifices μTCA shelf number, AMC slot number, and OptoHybrid number, and the inner dictionary uses (key, value) pairs of (register name, value), e.g.,

.. code-block:: python

   chamber_vfatDACSettings = {    
           (1,4,2):{
               "CFG_PULSE_STRETCH":3,
               "CFG_LATENCY":97,
               "CFG_RES_PRE":2,
               "CFG_CAP_PRE":1,
               },
           (1,4,3):{
               "CFG_PULSE_STRETCH":3,
               "CFG_LATENCY":98,
               "CFG_RES_PRE":2,
               "CFG_CAP_PRE":1,
               },
           (1,4,6):{
               "CFG_PULSE_STRETCH":3,
               "CFG_LATENCY":99,
               "CFG_RES_PRE":2,
               "CFG_CAP_PRE":1,
               }
       }

With these settings a call of ``confChamber.py`` will overwrite the values of:

* ``CFG_PULSE_STRETCH``,
* ``CFG_LATENCY``,
* ``CFG_RES_PRE``, and
* ``CFG_CAP_PRE``

registers in the :ref:`gemos-frontend-vfatcfgfile` for all VFATs for OptoHybrids 0 through 2. 


