
.. _gemos-operations-data-taking:

=======================================
Taking calibration & commissioning data
=======================================

Calibration and commissioning scans can be taken with the ``run_scans.py`` interface.

.. warning::
   The CTP7 generates the TTC stream or forwards a TTC stream from the AMC13.
   This means that all OptoHybrids connected to a given CTP7 receive a common TTC stream.
   Consequently, you cannot be taking data with routine ``A`` with ``OHX`` and simultaneously take data with routine ``B`` with ``OHY``.

.. important::
   Additionally the scan routines rely on several blocks of the CTP7 FW:

   * ``SBIT_MONITOR``
   * ``VFAT_DAQ_MONITOR``,
   * etc...

   These blocks can only receive information from one OptoHybrid at a time and thus almost all scans presently have to be run in series; they *cannot* be run in parallel.
   This *may* change as the SW tools develop.


Getting the VFAT mask: ``getVFATMask.py``
-----------------------------------------

In some cases you may not have a fully instrumented setup (e.g., fewer than 24 VFATs) or you may not be able to communicate with all 24 VFATs for one reason or another.
In this case you will need to mask either the non-responsive VFATs or those that are not physically present (otherwise scan applications will crash).

.. note::
   "Newer" scan applications will automatically determine which VFATs are good or bad for you.
   However, older scans will require you to provide a VFAT mask, a 24 bit number where the :math:`N^{th}` bit corresponds to the :math:`N^{th}` VFAT and a value of 1 indicates the VFAT should be skipped.

To make things simplier, especially for newer users a tool has been provided to determine the VFAT mask for you:

.. code-block:: bash

   getVFATMask.py -c eagleXX -gY

which should produce output:

.. code-block:: bash

   Open pickled address table if available  /opt/cmsgemos/etc/maps/amc_address_table_top.pickle...
   Initializing AMC eagleXX
   The VFAT Mask you should use for OHY is: <some hex number>

Here the ``getVFATMask.py`` tool was given ``eagleXX`` ``OHY`` and returns ``some hex number``, which is the VFAT mask for *this* OptoHybrid.


.. _gemos-usage-chamber-info:

The mapping file: ``chamberInfo.py``
------------------------------------

The ``chamberInfo.py`` file is a part of the ``gempython_gemplotting`` package, and is responsible for loading the required python dictionaries from your setup's ``system_specific_constants.py`` file at runtime.

.. code-block:: console

   locate system_specific_constants.py

which should produce output:

.. code-block:: bash

   /home/gemuser/gemdaq/config/system_specific_constants.py

.. important::
   If this file does not exist, you must create it based on the template from the ``chamberInfo.py`` file in the ``gemplotting`` package.
   This file will exist in the ``site-packages`` directory of your python environment:

   .. code-block:: bash

      python -c "import site; print(site.getsitepackages())"

   which should produce output:

   .. code-block:: console

      ['/usr/lib64/python2.7/site-packages', '/usr/lib/python2.7/site-packages', '/usr/lib/site-python']


   Within ``/usr/lib/python2.7/site-packages``, the file should be at ``gempython/gemplotting/mapping/chamberInfo.py``

   In your copied version, you should delete everything except for the definitions of

   * ``chamber_config``
   * ``chamber_GEBtype``
   * ``chamber_vfatDACSettings``

   which you should modify to match your setup.

.. warning::
   You should never have to change the ``chamberInfo.py`` file and you should only ever edit the ``system_specific_constants.py`` file.
   If you are operating a GEM managed setup, you should use the ``editConfig`` alias on the user account, e.g., call:

   .. code-block:: bash

      editConfig

Once the ``chamberInfo.py`` file has loaded the information from ``system_specific_constants.py``, at runtime the python API will have a set of dictionaries:

#. ``chamber_config``
#. ``GEBtype``
#. ``chamber_vfatDACSettings`` dictionary (see :ref:`gemos-chamber-vfat-dac-settings` to write common register values

These dictionaries:

* define which detectors are connected
* provide the mapping between detector serial numbers and geographic addresses
* possible DAC values to use to overwrite values stored in the :ref:`VFAT3 configuration file on the CTP7<gemos-frontend-vfatcfgfile>`
* the appropriate data paths to write output data to
* the chamber type

The keys of the ``chamber_config`` and ``GEBtype`` are the geographic address (e.g., ``ohKey``) in the form of a tuple ``(shelf,slot,link)``, which specifices μTCA shelf number, AMC slot number, and OptoHybrid number.
This enables you to define your test stand by specifying which detector is located at which geographic address and what its detector type is, i.e., "long", "short", "m1", etc...


.. _gemos-usage-run-scans:

Taking calibration data: ``run_scans.py``
-----------------------------------------

Once the dictionaries in ``chamberInfo.py`` are correctly configured, see:

* :ref:`gemos-chamber-vfat-dac-settings`, and
* :ref:`gemos-usage-chamber-info`

you're now able to use the command line interface tool ``run_scans.py`` for automatically launching scan applications for your test stand.
The ``run_scan.py`` will for each uncommented link (OptoHybrid number) in ``chamber_config`` dictionary:

* Create a scandate directory in the correct data path under: ``$DATA_PATH/chamber_config[ohN]``,
* Set the permissions on this directory so anyone in the ``gemuser`` group can ``write`` or ``read``,
* Call the scan application and configure it with the correct inputs,
* Create a log file of the scan, and
* Set the permissions on the output files so anyone in the ``gemuser`` group can read them.

The ``run_scans.py`` tool accepts different commands, each command represents a different scan application and has it's own positional (mandatory) and optional arguments.
You can calling the help menu of ``run_scans.py`` via:

.. code-block:: bash

   run_scans.py -h

will display each of the possible input commands and provide a brief description of them.
You can call the help menu of each of the commands in ``run_scans.py`` by calling:

.. code-block:: bash

   run_scans.py COMMAND -h

For example calling the help menu for the ``scurve`` command:

.. code-block:: sh

   run_scans.py scurve -h

and should produce output:

.. code-block:: bash

   usage: run_scans.py scurve [-h] [--chMax CHMAX] [--chMin CHMIN] [-l LATENCY]
                              [-m MSPL] [-n NEVTS] [--vfatmask VFATMASK]
                              cardName ohMask

   positional arguments:
     cardName              hostname of the AMC you are connecting to, e.g.
                           'gem-shelf01-amc02'
     ohMask                ohMask to apply, a 1 in the :math:`N^{th}` bit indicates the
                           :math:`N^{th}` OH should be considered

   optional arguments:
     -h, --help            show this help message and exit
     --chMax CHMAX         Specify maximum channel number to scan
     --chMin CHMIN         Specify minimum channel number to scan
     -l LATENCY, --latency LATENCY
                           Setting of CFG_LATENCY register
     -m MSPL, --mspl MSPL  Setting of CFG_PULSE_STRETCH register
     -n NEVTS, --nevts NEVTS
                           Number of events for each scan position
     --vfatmask VFATMASK   If specified this will use this VFAT mask for all
                           unmasked OH's in ohMask. Here this is a 24 bit number,
                           where a 1 in the :math:`N^{th}` bit means ignore the :math:`N^{th}` VFAT.
                           If this argument is not specified VFAT masks are taken
                           from chamber_vfatMask of chamberInfo.py


.. _gemos-usage-rcms:

Taking cosmic data: RCMS
------------------------

#. Navigate to the RCMS start page (for QC8 this is at gem904daq01.cern.ch:10000/rcms)
#. Enter the username and password
#. Verify that there are no currenly running configurations

   * If there are, and the configureation is the one you need to start, click on the link and select ``Attach``, and if the run is ``Running`` verify that it is OK to stop
   * If there is no currently running configuration, navigate to the ``Configuration Chooser`` and select the appropriate configuration, and click ``Create``

#. From this step, follow the procedure for your teststand to start a run

   * For QC8 this is:

     #. Initialize
     #. Configure
     #. Execute ``setDAQParamsPostConfigure.py`` from the command line with the appropriate options
     #. Start

Happy calibrating and commissioning!
