.. _gemos-amc13-guide:

==================
AMC13 user's guide
==================

The `AMC13 <http://iopscience.iop.org/article/10.1088/1748-0221/8/12/C12036/meta>`_ provides clock, timing, and DAQ service to the GEM μTCA crate either from the the TCDS system (at P5) or in local loopback mode (at a test stand).


.. _gemos-amc13-amc13tool:

Using ``AMC13Tool2.exe``
------------------------

The AMC13 can be configured by hand using the ``AMC13Tool2.exe`` command line interface (CLI).
To use this tool execute:

.. code-block:: bash

   AMC13Tool2.exe -i gem.shelfXX.amc13 -c $GEM_ADDRESS_TABLE_PATH/connections.xml

Here XX is the μTCA shelf number (e.g., ``XX = 01`` for most setups), note this is *always* represented with two digits even if the shelf number is less than 10.
This provides a command line interface for reading/writing registers of the AMC13 and querying the status of the systme.
An example of a successful execution of the above command looks like:

.. code-block:: bash

   AMC13Tool2.exe -i gem.shelf01.amc13 -c $GEM_ADDRESS_TABLE_PATH/connections.xml
   Address table path "/opt/cactus/etc/amc13/" set from AMC13_ADDRESS_TABLE_PATH
   Using .xml connection file...
   Using AMC13 software ver:50470
   Read firmware versions 0x2257 0x2e
   flavor = 2  features = 0x000000b2

This indicates that the address table being used is the `xml` file found in `/opt/cactus/etc/amc13/`.
It also indicates that the SW version being used corresponds to the `SVN` commit `50470` and that the firmware versions in use are `0x2257` (T1) and `0x2e` (T2)
You can see all available commands inside the ``AMC13Tool2.exe`` by executing ``help`` (or ``h``) from the tool CLI.
Help on subcommands can be displayed by executing ``h <subcommand>`` from the tool CLI.


Some useful commands are:

* ``st`` Display AMC13 Status (see :ref:`gemos-amc13-status`),
* ``en`` Enable an AMC slot (see :ref:`gemos-amc13-enabling-clock`),
* ``wu`` Create a register dump for Mr. Wu (AMC13 developer) to analyze (see :ref:`gemos-amc13-wudump`),


.. _gemos-amc13-status:

Checking status of a given crate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To check the status of a particular AMC13 enter the ``AMC13Tool2.exe`` and execute one of four options:


* ``st`` displays the generic status menu
* ``st 2`` as ``st`` but shows additional information about enabled AMC slots,
* ``st 3`` as ``st 2`` but also shows clock frequency information and FPGA voltage and temperature
* ``st 4`` as ``st 3`` but with significantly more information
* ``st 99`` shows all status information (large text dump)


.. _gemos-amc13-enabling-clock:

Enabling clock to an AMC slot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To enable 

.. code-block:: bash

   AMC13Tool2.exe -i gem.shelfXX.amc13 -c $GEM_ADDRESS_TABLE_PATH/connections.xml
   ws CONF.TTC.OVERRIDE_MASK 0xfff
   en <slots> t

Here XX is the μTCA shelf number (e.g., ``XX = 01`` for most setups), note this is *always* represented with two digits even if the shelf number is less than 10.
The second command ensures all slots have a clock.
The third command will enable the slots of interest and place the AMC13 in TTC loop back mode (drop the ``t`` for P5 operation, or whenver the TTC stream is coming from TCDS).
Here ``<slots>`` is a comma and dash separated list, e.g., ``en 2-5,7 t`` will enable slots 2 *through* 5 and slot 7.


.. _gemos-amc13-wudump:

Dumping current register information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In some cases you might need to make a dump of all information on the AMC13 (e.g., to see hardware configuration after a particular problem has occurred).
To do this execute:

.. code-block:: bash

   >wu

This will dump the current configuration to a text file and in the terminal output the filepath will be printed.
