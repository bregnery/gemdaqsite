.. _gemos-gem-reg-cli:

============
Register CLI
============

The ``gem_reg.py`` tool is a command line interface (CLI) that allows you to perform several actions on GEM electronics hardware:

#. Register read/write
#. Getting information about a register


.. _gemos-cli-usage:

Using ``gem_reg.py``
--------------------

To get started you should first open a connection to the CTP7 of interest.
This is a two-step process, the first is to start the CLI tool ``gem_reg.py``, which should result in the following output:

.. code-block:: bash

   gem_reg.py
   % gem_reg.py
   Open pickled address table if available  /opt/cmsgemos/etc/maps/amc_address_table_top.pickle...
   Starting CTP7 Register Command Line Interface. Please connect to CTP7 using connect <hostname> command unless you use it directly at the CTP7
   CTP7 >

Once the tool is activated, you can see available CLI commands by looking at the ``help`` menu.
This is viewable by calling the ``help``.
The second step is to call the ``connect`` function of the CLI with the hostname of the CTP7 of interest as the argument:

.. code-block:: bash

   CTP7 > connect gem-shelfXX-amcYY
   gem-shelfXX-amcYY >

You will now have opened an rpc connection to the CTP7 whose network alias is ``gem-shelfXX-amcYY``.

.. note::

   The ``rpcsvc`` service must be running on the CTP7 and *owned* by the ``gemuser`` account (not the ``texas`` account).
   If ``rpcsvc`` is not running or it is running and owned by the ``texas`` account you may find the connection fails.


.. _gemos-cli-commands:

Useful commands
~~~~~~~~~~~~~~~

You can see available commands by looking at the ``help`` menu, i.e., entering ``help`` at the CLI prompt..

Some of the most useful commands are:

+-----------------+-------------------------------------------------+-----------------------------------+
| Command         | Description                                     | Example usage                     |
+=================+=================================================+===================================+
| ``connect``     | opens an RPC connection to a CTP7               | .. code-block:: bash              |
|                 |                                                 |                                   |
|                 |                                                 |    connect gem-shelf01-amc02      |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``doc``         | prints additional information about a register  | .. code-block:: bash              |
|                 |                                                 |                                   |
|                 |                                                 |    doc <full node name>           |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``exit``        | exits the CLI (or press ``Ctrl+D``)             |                                   |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``help``        | prints a general or a command specific help     | .. code-block:: bash              |
|                 | menu                                            |                                   |
|                 |                                                 |    help                           |
|                 |                                                 |    help <cmd>                     |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``kw``          | reads all node names containing a substring     | .. code-block:: bash              |
|                 |                                                 |                                   |
|                 |                                                 |    kw OH0.GEB.VFAT21.CFG          |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``rwc``         | reads all node names matching the string,       | .. code-block:: bash              |
|                 | segmented by the wildcard character ``*``       |                                   |
|                 |                                                 |   rwc GEM_AMC*OH0*VFAT*CFG_RUN    |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``read``        | reads a given node name                         | .. code-block:: bash              |
|                 |                                                 |                                   |
|                 |                                                 |    read <full node name>          |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``readAddress`` | reads a given address and displays register     | .. code-block:: bash              |
|                 | value                                           |                                   |
|                 |                                                 |    readAddress 0xdeadbeef         |
+-----------------+-------------------------------------------------+-----------------------------------+
| ``write``       | writes a value to a register specified by node  | .. code-block:: bash              |
|                 | name                                            |                                   |
|                 |                                                 |    write <full node name>         |
+-----------------+-------------------------------------------------+-----------------------------------+

Here a node is a particular point in the xml address table, nodes typically go as ``string1.string2.string3`` and so on.
Here ``string2`` is the parent node of ``string3`` and the daughter node of ``string1``.

.. warning::

   At any time, if you press ``Return`` from the CLI when there is no input at the prompt, it will re-execute the last command that was entered

.. note::
   While running ``gem_reg.py``, when issuing a ``KeyboardInterrupt`` (i.e., pressing ``Ctrl+C``) this will *not* terminate ``gem_reg.py``, but it *will* kill the rpc connection, a new connection must be opened with the ``connect`` command afterward.


.. _gemos-cli-reg-info:

Getting info about a register
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note::
   All registers in the CTP7 address space are 32-bit registers but a single register may be shared by multiple named nodes

To get the documentation for a given register you need to call ``doc`` on the full node name, for example:

.. code-block:: bash

   eagle26 > doc GEM_AMC.OH.OH2.GEB.VFAT23.CFG_RUN
   Name: GEM_AMC.OH.OH2.GEB.VFAT23.CFG_RUN
   Description: SLEEP/RUN mode (0 = SLEEP, 1 = RUN)
   Address: 0x0052bb00
   Permission: rw
   Mask: 0x00000001
   Module: False
   Parent: GEM_AMC.OH.OH2.GEB.VFAT23
   None

The meaning of the fields described in this table

+-----------------+------------------------------------------------------------+
| Field name      | Description                                                |
+=================+============================================================+
| ``Name``        | the full node of the register in the address table         |
+-----------------+------------------------------------------------------------+
| ``Description`` | the node documentation                                     |
+-----------------+------------------------------------------------------------+
| ``Address``     | the address of the register in the CTP7 address space      |
|                 | corresponding to the named node                            |
+-----------------+------------------------------------------------------------+
| ``Permissions`` | indicates whether the node addressed is read-only (``r``), |
|                 | write-only (``w``), or read-write (``rw``)                 |
+-----------------+------------------------------------------------------------+
| ``Mask``        | indicates which bits of the register at the specified      |
|                 | address the named node occupies.                           |
+-----------------+------------------------------------------------------------+
| ``Parent``      | the parent node in the address table tree                  |
+-----------------+------------------------------------------------------------+

.. warning::
   If you write directly to a raw address without using the node name, you *must* to carefully apply the mask or you risk changing the value of other nodes that share the same 32-bit register.
