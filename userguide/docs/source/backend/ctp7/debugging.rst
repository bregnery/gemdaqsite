.. _gemos-ctp7-debugging:

==============================
Debugging back-end electronics
==============================

Below are some tips and suggestions for how to start debugging some issues you may encounter.
They are provided as a reference, as you may be asked to perform one or more of these actions by the DAQ expert.
They are also useful to help provide additional information to the DAQ expert when you report a problem.

.. _gemos-ctp7-connectivity:

Connecting directly to the CTP7
-------------------------------

Most actions do not ever require you to login to the CTP7 and they are performed from the DAQ PC itself.
However, some actions may require that you login to the CTP7.
To do this, execute the following from the DAQ machine:

.. code-block:: bash

   ssh gemuser@gem-shelfXX-amcYY

where ``gem-shelfXX-amcYY`` is the network alias of the CTP7 of interest, e.g., ``gem-shelf01-amc02``.
This will attempt to log in to the CTP7 in slot 2 of the μTCA shelf configured as shelf 1.

.. important::

   Any action performed on a CTP7 should be recorded in full on the elog that corresponds to the system the card is on.
   See :ref:`gemos-elogs` for details on which elog is of interest and how to make a proper elog.


.. _gemos-ctp7-rpcsvc:

The ``rpcsvc`` Service
^^^^^^^^^^^^^^^^^^^^^^

Most common actions involving the CTP7 requie the Remote Procedure Call (RPC) service to be running and owned by the ``gemuser`` account.
This service is run as a system process called ``rpcsvc``.
To check if this process is running login to the CTP7 and execute:

.. code-block:: bash

   ps | grep [r]pcsvc

Example output should look like:

.. code-block:: bash

   10994 gemuser   4040 S    rpcsvc

If you only see a line that says ``grep rpcsvc`` then the ``rpcsvc`` service is *not* running.
Additionally if you see that ``rpcsvc`` is running but it is not owned by the ``gemuser`` account then it will be configured with the *wrong* environment and must be killed (either by the DAQ expert using the ``root`` account or from a ``$USER`` of the owning account).

.. note::

   You might find that multiple lines are returned which show multiple ``rpcsvc`` services.
   If these are all owned by ``gemuser`` then this just indicates that the ``rpcsvc`` service is running and that one or more open ``rpc`` connections exist between the card and the DAQ PC.

Sometimes you may need to restart the ``rpcsvc`` (typically after updating the ``rpcmodules`` on the card), to do this execute:

.. code-block:: bash

   killall rpcsvc && rpcsvc


.. _gemos-ctp7-dirs:

CTP7 directories
^^^^^^^^^^^^^^^^

The ``/mnt/persistent/gemdaq/vfat3`` directory contains the per ``(ohN,vfatN)`` configuration file specifying registers per chip (e.g., here you would edit the ``CFG_IREF`` for the VFAT of interest).
Inside the ``vfat3`` directory there will be a set of configuration files and symlinks that are given by the following pattern:

.. code-block:: bash

   lrwxrwxrwx    1 gemuser  1001            53 Aug 10 15:27 config_OHX_VFATY.txt -> /mnt/persistent/gemdaq/vfat3/config_OHX_VFATY_cal.txt
   -rw-r--r--    1 gemuser  1001          1267 Aug 10 15:27 config_OHX_VFATY_cal.txt

The symlink is what is used by the configuration command to configure the ``vfat3`` in ``(ohN,vfatN) = (X,Y)`` position; so this must always be a valid link.
An example of how this file is expected to look can be found in the section :ref:`gemos-frontend-vfatcfgfile`.

The address table ``xml`` files will be found under the ``/mnt/persistent/gemdaqxml`` folder and a set of symlinks will be specified there, for example:

.. code-block:: bash

   -rw-r--r--    1 51446    1399      21149299 Aug  8 15:46 gem_amc_top.pickle
   lrwxrwxrwx    1 51446    1399            18 Aug  8 15:45 gem_amc_top.xml -> gem_amc_v3_5_3.xml
   -rw-r--r--    1 51446    1399        136064 Aug  8 15:45 gem_amc_v3_5_3.xml
   -rw-r--r--    1 51446    1399        102472 Aug  8 15:41 oh_registers_3.1.2.B.xml
   lrwxrwxrwx    1 51446    1399            24 Aug  8 15:41 optohybrid_registers.xml -> oh_registers_3.1.2.B.xml


.. _gemos-ctp7-lmdb:

RPC Modules and the LMDB
^^^^^^^^^^^^^^^^^^^^^^^^

The ``rpcmodules`` and ``LMDB`` are two terms you may hear when interacting eith the DAQ expert.
These two pieces are how the functions perform register access.

The LMDB is a database that the ``rpcmodules`` query when functions are launched on the CTP7.
The ``rpcmodules`` are scoped by their functionality, depending on which part of the system they interact with.

To view logging information that may help point at the source of some problems, on the CTP7 the logfile

.. code-block:: bash

   /var/log/messages

On the DAQ PC (if it is configured as the ``sysmgr`` host) the log files for all connected cards can be found under ``/var/log/remote``, e.g.,:

.. code-block:: bash

   /var/log/remote/gem-shelfXX-amcYY/messages.log

Where ``gem-shelfXX-amcYY`` is the network alias of the CTP7 (e.g., ``gem-shelf01-amc02``).
You can see the most recent information found in the log file via:

.. code-block:: bash

   tail -25 /var/log/messages

This will display the last 25 lines in the log file on the CTP7 (executed as ``gemuser`` on the CTP7).
Use the ``tail`` command similarly to view the log on the DAQ PC.

If the log file is either not present, or the information is stale (not updating), contact the GEM DAQ expert to resolve the problem.
