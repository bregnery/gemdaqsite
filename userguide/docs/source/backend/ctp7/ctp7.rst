.. _gemos-ctp7-guide:

=================
CTP7 user's guide
=================

The `Calorimeter Trigger Processor 7 <http://iopscience.iop.org/article/10.1088/1748-0221/11/02/C02011/meta>`_, or CTP7 for short, is a μTCA AMC used by several subsystems of CMS.

The CTP7 runs a 32-bit version of Linux on an embedded processor called a Zynq, which features extremely fast register access to the Virtex 7 FPGA.
For more in-depth information on the CTP7, see :ref:`this overview in the expert guide<expertguide:gemos-ctp7-overview>`.

In the GEM project it is the present back-end AMC for GE1/1.
It is responsible for slow control (register read/write), tracking data readout, and event building.
In the case of GE1/1, it controls up to 12 OptoHybrids (but the FW version your card is using may be compiled for fewer).

.. toctree::
   :caption: Common operations
   :maxdepth: 5

   gem-reg
   debugging
