.. CMS GEM Online Software documentation master file, created by
   sphinx-quickstart on Wed Mar  4 08:42:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==============================
CMS GEM DAQ user documentation
==============================

Hello!
Congratulations, you're taking one of the first steps to becoming an expert on the GEM DAQ electronics.
For you to get the most out of this guide there's a couple of things that we should discuss first.
The first is how this guide should be used.
It's broken down into several sections, each section focuses on a specific topic.
Linked here you will find various information related to the GEM online software and DAQ operations.

Now go ahead and choose the topic you were looking for information on, or use the search feature.


.. toctree::
   :caption: Infrastructure
   :maxdepth: 3

   Teststands  <usage/teststands>

.. toctree::
   :caption: Operations
   :maxdepth: 3

   Common slow control actions <operations/slow-control>
   Configuring a detector <operations/configure-detector>
   Taking calibration & commissioning data <operations/taking-data>

.. toctree::
   :caption: Electronics overview
   :maxdepth: 3

   Back-end electronics  <backend/backend>
   Front-end electronics <frontend/frontend>

.. toctree::
   :caption: Getting help
   :maxdepth: 1

   FAQ <faq>
   Contact <contact>
   Reporting bugs <bugs>

.. toctree::
   :hidden:
   :caption: Links to other documentation
   :maxdepth: 1

   CMS GEM DAQ project page <http://0.0.0.0:8000>
   Users guide (this site) <http://0.0.0.0:8000/SITE_ROOT/guides/userguide>
   Expert guide <http://0.0.0.0:8000/SITE_ROOT/guides/expertguide>
   Developer guide <http://0.0.0.0:8000/SITE_ROOT/guides/devguide>

.. toctree::
   :hidden:
   :caption: Links to API documentation
   :maxdepth: 1

   cmsgemos <http://0.0.0.0:8000/SITE_ROOT/docs/api/cmsgemos/latest>
   gemplotting <http://0.0.0.0:8000/SITE_ROOT/docs/api/gemplotting/latest>
   vfatqc <http://0.0.0.0:8000/SITE_ROOT/docs/api/vfatqc/latest>
   ctp7_modules <http://0.0.0.0:8000/SITE_ROOT/docs/api/ctp7_modules/latest>
   reg_utils <http://0.0.0.0:8000/SITE_ROOT/docs/api/reg_utils/latest>
   xhal <http://0.0.0.0:8000/SITE_ROOT/docs/api/xhal/latest>


Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`
