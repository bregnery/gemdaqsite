.. _gemos-teststand-guide:

===================
Teststand etiquette
===================

It is important to realize we are all sharing a set of common equipment and are working towards a common goal.
Furthermore you and your coworkers may have different skill levels and possess varying degrees of familiarity with the hardware and the software.
This brings us to the first important point:

.. important::
   **Leave the teststand how you found it**.

   This specifically refers to the:

   #. Teststand infrastructure (e.g., fiber patch panels, power supplies, DAQ computer, etc...), and
   #. State of software (e.g., ``rpcmodules``) and firmware of backend electronics.

It can be expected that you would need to configure the front-end electronics for whatever test/development you are working on.
But the back-end electronics should *always* be left in a usable state for the next user (i.e., at worst, exactly how *you* found them).

Moreover, if you are not on the list of approved individuals who can modify the hardware/stand infrastructure you should not (if you are wondering if you are on this list it probably means you are *not*).

Failure to follow these rules makes more work for the DAQ expert team, sysadmin/teststand responsible.
Failure to conform repeatedly may result in loss of access to the teststand.

Once you realize this you should:

#. Determine the right teststand to use :ref:`gemos-available-teststands`
#. Request time on a teststand :ref:`gemos-teststand-booking`
#. Use the appropriate e-log :ref:`gemos-elogs` to log all activities

.. _gemos-available-teststands:

Available teststands & their uses
----------------------------------

The following 904 teststands exist


+--------------------+-----------------------+-----------------------------------------+
| Stand              | Location              | Purpose                                 |
+====================+=======================+=========================================+
| QC8                | GEM 904 lab           | Production teststand for                |
|                    |                       | GE1/1 qualification                     |
+--------------------+-----------------------+-----------------------------------------+
| QC7                | GEM 904 lab           | Electronics quality control             |
|                    |                       | and chamber assembly QC                 |
+--------------------+-----------------------+-----------------------------------------+
| V3 Electronics R&D | GEM 904 lab           | Testing sustained operations            |
|                    |                       | of GE1/1 detectors                      |
+--------------------+-----------------------+-----------------------------------------+
| Coffin             | 904 integration lab   | General purpose debugging,              |
|                    |                       | support P5 operations,                  |
|                    |                       | software development and                |
|                    |                       | testing, firmware testing,              |
|                    |                       | trigger development, CSC integration    |
+--------------------+-----------------------+-----------------------------------------+


Unless you are involved in, or performing a test for, the sustained operations group or QC8 for GE1/1 qualification you should default to using the "Coffin" setup.


Teststand infrastructure
------------------------

Mainframes, fiber patch panels, μTCA crate numbers & names, AMC's, etc...

.. _gemos-elogs:

Electronic logbook (e-log)
--------------------------

For each of the teststands described in the section :ref:`gemos-available-teststands`, a corresponding electronic logbook exists, as shown in the table below:

.. list-table::
   :header-rows: 1

   * - Stand
     - E-Log
   * - "Coffin"
     - `904 Integration <https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog?__adfpwp_action_portlet=623564097&__adfpwp_backurl=https%3A%2F%2Fcmsonline.cern.ch%3A443%2Fwebcenter%2Fportal%2Fcmsonline%2Fpages_common%2Felog%3FMedia-Type%3Dscreen%26Media-Feature-Scan%3D0%26Media-Feature-Orientation%3Dlandscape%26Media-Feature-Device-Height%3D1050%26Media-Feature-Height%3D789%26_afrWindowMode%3D0%26Media-Feature-Monochrome%3D0%26Font-Size%3D16%26Media-Feature-Color%3D8%26Media-Featured-Grid%3D0%26_afrLoop%3D12894451140606290%26Media-Feature-Resolution%3D192%26Media-Feature-Width%3D1680%26Media-Feature-Device-Width%3D1680%26Media-Feature-Color-Index%3D0%26Adf-Window-Id%3Dw0%26__adfpwp_mode.623564097%3D1&_piref623564097.strutsAction=%2FviewSubcatMessages.do%3FcatId%3D791%26subId%3D799%26page%3D1>`_
   * - V3 Electronics R&D
     - `V3 Electronics Testing <https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog?__adfpwp_action_portlet=623564097&__adfpwp_backurl=https://cmsonline.cern.ch:443/webcenter/portal/cmsonline/pages_common/elog?__adfpwp_mode.623564097=1&_piref623564097.strutsAction=//viewSubcatMessages.do?catId=791&subId=1511&page=1&fetch=1&mode=expanded>`_
   * - QC8
     - `Cosmic Stand <https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog?__adfpwp_action_portlet=623564097&__adfpwp_backurl=https://cmsonline.cern.ch:443/webcenter/portal/cmsonline/pages_common/elog?_piref623564097.strutsAction=//viewSubcatMessages.do?catId=792&subId=793&page=1&fetch=1&mode=expanded>`_


When using a teststand you should:

#. Open an elog once you start using the stand,
#. Show commands executed, and when relevant their outputs, 
#. Summarize the actions taken and the result(s)/problem(s) encountered, and
#. State when you are finished using the stand

.. _gemos-teststand-booking:

Requesting time on GEM teststands
---------------------------------

Each stand has its own requisition page on SuperSAAS to manage testing and ensure we do not collide with other users.
To see the available teststands and to request time on navigate to:

https://www.supersaas.com/schedule/GEM_904_Infrastructure

If you need time on a particular setup you need to understand what hardware you will be using.
Will this be just the front-end(s) on a given link? In this case you'll need time on the AMC in question.
Will testing involved the front-end being triggered from a trigger source coming from AMC13? Then you'll need time on the μTCA crate in question.

Before trying to modify the above schedules you'll need to first ask for the GEM 904 Shared User Password on SuperSAAS to use the scheduling tools.
To do this ask in the ``System Setup`` channel of the `GEM DAQ Mattermost Team <https://mattermost.web.cern.ch/signup_user_complete/?id=ax1z1hss5fdm8bpbx4hgpoc3ne>`_.
When scheduling you need to provide:

* Your name
* Your email
* Estimated time your test will take (starting & ending time)
* Phone number you are reachable at while using the teststand
* Description of your test

Your request will be submitted and then approved.

.. note::
   You should only use the stand once the request has been *approved*.
   Once your request has been approved and you start using the stand you still are require to make an elog entry documenting the actions you have taken, their outcome, and relevant commands, etc...
