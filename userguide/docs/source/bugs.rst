.. _gemos-bugs:

==============
Reporting Bugs
==============

.. tabs::

   .. tab:: Legacy SW

      +----------------------------+------------------------------------------------------------+
      | Package Name               | Bug Tracker                                                |
      +============================+============================================================+
      | ``cmsgemos`` (``xdaq``)    | gitlab.cern.ch/cms-gem-daq-project/cmsgemos                |
      +----------------------------+------------------------------------------------------------+
      | ``cmsgemos_gempython``     | gitlab.cern.ch/cms-gem-daq-project/cmsgemos                |
      +----------------------------+------------------------------------------------------------+
      | ``gempython_vfatqc``       | gitlab.cern.ch/cms-gem-daq-project/vfatqc-python-scripts   |
      +----------------------------+------------------------------------------------------------+
      | ``gempython_gemplotting*`` | gitlab.cern.ch/cms-gem-daq-project/gem-plotting-tools      |
      +----------------------------+------------------------------------------------------------+
      | ``xhal``                   | gitlab.cern.ch/cms-gem-daq-project/xhal                    |
      +----------------------------+------------------------------------------------------------+
      | ``ctp7_modules``           | gitlab.cern.ch/cms-gem-daq-project/ctp7_modules            |
      +----------------------------+------------------------------------------------------------+

   .. tab:: non-legacy

