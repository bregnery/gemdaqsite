.. _gemos-frontend-vfatcfgfile:

===============================
VFAT configuration file on CTP7
===============================

The ``confChamber.py`` tool, see :ref:`gemos-operations-conf-det`, can be used to apply a common DAC setting to *all* VFATs for a given register.

The script ``replace_paramater.sh`` can be used to apply a unique, i.e., per-VFAT, value to a given register.
The script must be run as user ``gemuser`` on the CTP7 in order to have permission to edit the configuration files.
The per-VFAT mode of the script is invoked with

.. code-block:: bash

   ./replace_parameter.sh -f <FILENAME> <REGISTER> <LINK>

where the input file format is the same as the output of ``anaDACScan.py``.

As an example

.. code-block:: bash

   ./replace_parameter.sh -f ~/NominalDACValues_GE11-X-S-INDIA-0002/2018.10.31.14.27/NominalDACValues.txt BIAS_PRE_VREF 0

will cause each line of the file ``~/NominalDACValues_GE11-X-S-INDIA-0002/2018.10.31.14.27/NominalDACValues.txt`` to be parsed, and if, for example, one line of the file is ``6 102``, then the value of the register ``BIAS_PRE_VREF`` will be replaced by ``102`` in the config file for VFAT ``6``.

The VFAT configuration files can also be manually edited, an example is shown below:

.. code-block:: bash

   more vfat3/config_OH3_VFAT9_cal.txt

.. code-block:: bash

   dacName/C:dacVal/I
   PULSE_STRETCH           7
   SYNC_LEVEL_MODE         0
   SELF_TRIGGER_MODE       0
   DDR_TRIGGER_MODE        0
   SPZS_SUMMARY_ONLY       0
   SPZS_MAX_PARTITIONS     0
   SPZS_ENABLE             0
   SZP_ENABLE              0
   SZD_ENABLE              0
   TIME_TAG                0
   EC_BYTES                0
   BC_BYTES                0
   FP_FE                   7
   RES_PRE                 1
   CAP_PRE                 0
   PT                     15
   EN_HYST                 1
   SEL_POL                 1
   FORCE_EN_ZCC            0
   FORCE_TH                0
   SEL_COMP_MODE           1
   VREF_ADC                3
   MON_GAIN                0
   MONITOR_SELECT          0
   IREF                   32
   THR_ZCC_DAC            10
   THR_ARM_DAC           200
   HYST                    5
   LATENCY                45
   CAL_SEL_POL             1
   CAL_PHI                 0
   CAL_EXT                 0
   CAL_DAC               200
   CAL_MODE                1
   CAL_FS                  0
   CAL_DUR               200
   BIAS_CFD_DAC_2         40
   BIAS_CFD_DAC_1         40
   BIAS_PRE_I_BSF         13
   BIAS_PRE_I_BIT        150
   BIAS_PRE_I_BLCC        25
   BIAS_PRE_VREF          86
   BIAS_SH_I_BFCAS       250
   BIAS_SH_I_BDIFF       150
   BIAS_SH_I_BFAMP         0
   BIAS_SD_I_BDIFF       255
   BIAS_SD_I_BSF          15
   BIAS_SD_I_BFCAS       255
