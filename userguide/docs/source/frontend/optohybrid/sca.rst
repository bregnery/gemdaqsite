.. _gemos-frontend-sca:

-----------------------
Slow Control ASIC (SCA)
-----------------------

.. toctree::
   :maxdepth: 1

   Voltage & Temperature Monitoring <sca-conversion>

The ``sca.py`` tool
^^^^^^^^^^^^^^^^^^^

The ``sca.py`` is a command line tool for sending a variety of commands to the SCA.
For a description of the possible commands and their calling syntax execute ``sca.py -h`` for more information.

.. _gemos-sca-reset:

Issuing an SCA reset
^^^^^^^^^^^^^^^^^^^^

To issue an SCA reset execute the following from the DAQ PC:

.. code-block:: bash

   sca.py r cardName ohMask

For example:

.. code-block:: bash

   sca.py r eagle60 0x3

This will issue an SCA reset to OH's 0 and 1 on ``eagle60``.

If a red error message appears for one or more of the OH's in your ``ohMask`` re-issue the SCA reset until no red error messages appear.
For subsequent SCA resets issued in this way you can use the same ``ohMask`` or modify it to remove the healthy OH's.
If continuing to issue an sca reset does not resolve the issue (i.e., red error messages continue to appear) there is a problem and you probably lost communication.
In this case check the status of ``GBT0`` on each of the problem GBTs using instructions under :ref:`gemos-gbt-ready-registers`, if GBTX is either not ready or was not ready you may need to either issue a GBTx link reset (see :ref:`gemos-gbt-link-reset`), re-program GBT0 (see :ref:`expertguide:gemos-gbt-programming`), or in rare cases power cycle and start from scratch.


.. _gemos-sca-status:

Checking the SCA status
^^^^^^^^^^^^^^^^^^^^^^^

There are two registers of great importance for checking SCA communication.
They are:

.. code-block:: bash

   GEM_AMC.SLOW_CONTROL.SCA.STATUS.READY
   GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR

Each is a 32-bit register where the lowest 12 map to the 12 OptoHybrids, with the :math:`N^{th}` bit corresponding to the :math:`N^{th}` OptoHybrid.
In the case of ``READY`` if the :math:`N^{th}` bit is high (1) it means the link is ready and communication is most likely stable (although in some cases the ``READY`` bit for a given OptoHybrid is 1 but slow control is not possible).
In the case of ``CRITICAL_ERROR`` if the :math:`N^{th}` bit is high (1) it means the SCA controller on the :math:`N^{th}` OptoHybrid has encountered a critical error and needs an SCA reset.


Using ``amc_info_uhal.py``
~~~~~~~~~~~~~~~~~~~~~~~~~~

You can get the SCA status on all optohybrids on a CTP7 from ``amc_info_uhal.py`` command.
Execute:

.. code-block:: bash

   amc_info_uhal.py --shelf=X -sY

The relevant SCA output for all OptoHybrids connected to the CTP7 in slot ``Y`` of shelf ``X`` will look like:

.. code-block:: bash

   --=======================================--
   -> GEM SYSTEM SCA INFORMATION
   --=======================================--

   READY             0x000003fc
   CRITICAL_ERROR    0x00000000
   NOT_READY_CNT_OH00 0x00000001
   NOT_READY_CNT_OH01 0x00000001
   NOT_READY_CNT_OH02 0x00000002
   NOT_READY_CNT_OH03 0x00000002
   NOT_READY_CNT_OH04 0x00000002
   NOT_READY_CNT_OH05 0x00000002
   NOT_READY_CNT_OH06 0x00000002
   NOT_READY_CNT_OH07 0x00000002
   NOT_READY_CNT_OH08 0x00000002
   NOT_READY_CNT_OH09 0x00000002
   NOT_READY_CNT_OH10 0x00000001
   NOT_READY_CNT_OH11 0x00000001

.. note::
   The ``ipbus`` service must be running on the CTP7, otherwise the command above will fail.


Using ``gem_reg.py``
~~~~~~~~~~~~~~~~~~~~

You can get the SCA status on all optohybrids on a CTP7 from ``gem_reg.py`` using the following command, with example output:

.. code-block:: bash

   eagleXX > rwc SCA*READY
   0x66c00400 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.READY                   0x00000002
   0x66c00408 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH0       0x00000001
   0x66c0040c r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH1       0x00000002
   0x66c00410 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH2       0x00000001
   0x66c00414 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH3       0x00000001
   eagle60 > read GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR
   0x66c00404 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR          0x00000000

Here we see that SCA READY is ``0x2`` so only OH1 has stable communication and no links are in error (critical error is ``0x0``).

.. note::
   The ``rpcsvc`` must be running on the CTP7, otherwise the command above will fail.


VFAT reset lines
^^^^^^^^^^^^^^^^

In the OHv3c version, the VFAT reset lines are controlled by the SCA and not the OH FPGA.
In CTP7 firmware versions higher than 3.5.2, the resets will automatically be lifted and no user action is required.
On older CTP7 FW versions to lift the VFAT resets first program the OHv3c FPGA following :ref:`these instructions<gemos-optohybrid-programming>` then execute (from the DAQ machine):

#. Configure the GPIO on the SCA

   .. code-block:: bash

      sca.py eagleXX ohMask gpio-set-direction 0x0fffff8f
      sca.py eagleXX ohMask gpio-set-output 0xf00000f0

#. Send a GBTx link reset (see :ref:`these instructions<gemos-gbt-link-reset>`)
#. Using the ``gem_reg.py`` tool

   #. Check the status of the GBT's, e.g., for ``OH0``

      .. code-block:: bash

         kw OH_LINKS.OH0.GBT

   #. Report the status of link good of all VFATs connected to ``OH0``

      .. code-block:: bash

         kw OH_LINKS.OH0.LINK_GOOD

   #. Report the status of sync error counters of all VFATs connected to ``OH0``

      .. code-block:: bash

         rwc OH0.VFAT*.SYNC_ERR_CNT

   #. Check for slow control of all VFATs

      .. code-block:: bash

         rwc OH0.*.CFG_RUN

   .. warning::
      The examples above will return the status of the 24 VFATs connected to ``OH0``, you should modify them as appropriate for the setup you are operating
