.. _gemos-frontend-feast:


=======
FEASTMP
=======

The `FEASTMP <https://project-dcdc.web.cern.ch/project-dcdc/public/Documents/FEASTMod_Datasheet.pdf>`_ is a radiation hard DC-DC power converter used by many systems at CERN to convert high input voltage into low output voltage.
The GE1/1 design (both long & short detectors) each use 10 FEASTs:


+------------+------------------+--------------------------------+
| Identifier | Provided voltage | Description                    |
+============+==================+================================+
| F1         | 1.0V             | Provides FPGA core voltage     |
+------------+------------------+--------------------------------+
| F2         | 2.55V            | Provide power to VTTx/VTRx     |
+------------+------------------+--------------------------------+
| F3         | 1.55V            | Provide power to GBTx and SCA  |
|            |                  | chips                          |
+------------+------------------+--------------------------------+
| F4         | 1.86V            | Provide power to EPROM         |
|            |                  | (not loaded in OHv3c systems)  |
+------------+------------------+--------------------------------+
| F5         | ???              | N/A                            |
+------------+------------------+--------------------------------+
| F6         | ???              | N/A                            |
+------------+------------------+--------------------------------+
| FQA, FQB,  | 1.2V             | Provide digital and analog     |
| FQC, FQD   |                  | power to 6 VFAT3s each         |
+------------+------------------+--------------------------------+

.. important::
   For table/benchtoop operation, it is recommended to apply heat sinks to *all* FEASTs and specifically to air cool with a fan F1 and F2.
