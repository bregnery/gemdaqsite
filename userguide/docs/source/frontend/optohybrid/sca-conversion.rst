.. _gemos-frontend-sca-conversion:

================================
ADC counts to voltage conversion
================================

The output values are 12 bit ADC and the voltage value varies from 0 to 1 V.
Thus, each bit corresponds to :math:`\frac{1\mathrm{V}}{2^{12}} \approx 0.244 \mathrm{mV}`.

.. _gemos-sca-pt100-sensors:

PT100/PT1000 Sensors
--------------------

Mounted at several locations on the OptoHybrid PCB are several PT1000 (PT100 in earlier versions) resistors.
These resistors have a well-known dependence on temperature, and are therefore a good way to measure temperatures.


Example conversion
~~~~~~~~~~~~~~~~~~

* Assume an ADC reading of 44 counts
* The corresponding voltage = :math:`44 \times 0.244 \mathrm{mV} = 10.736 \mathrm{mV}`
* The PT100 measures temperature using a platinum resistor through which :math:`100 \mu A` of current passes
* Resistance = :math:`V/I = 10.736\mathrm{mV} / 100 \mu A = 107.36 \Omega`
* Check the temperature corresponding to the :math:`107.36 \Omega` resistance in the R--T table [#pt100]_.
  This corresponds to :math:`19 C`.

.. _gemos-sca-internal-temperature:

SCA internal temperature
------------------------


* ADC count = 2743
* Corresponding voltage = :math:`2743 \times 0.244 \mathrm{mV} = 669.68 \mathrm{mV}`
* Now look at the V-T graph.
  The voltage of 669.68 corresponds to ~:math:`25 C`.

.. image:: https://user-images.githubusercontent.com/5220316/58358921-32d1c500-7e81-11e9-8ae6-ae5c3a6805df.png
   :target: https://user-images.githubusercontent.com/5220316/58358921-32d1c500-7e81-11e9-8ae6-ae5c3a6805df.png
   :alt: SCA V-T graph

The above V-T graph is taken from page 52 of the GBT-SCA manual [#gbtsca]_

.. _gemos-sca-adc-voltage-sensors:

Conversion of ADC for voltage sensors
-------------------------------------


* ADC count = 1025
* A multiplication factor of 4 is used, due to the voltage divider circuit having values :math:`3k\Omega` and :math:`1k\Omega`, respectively.

.. image:: https://user-images.githubusercontent.com/5220316/58358934-48df8580-7e81-11e9-83c6-d29534acf581.png
   :target: https://user-images.githubusercontent.com/5220316/58358934-48df8580-7e81-11e9-83c6-d29534acf581.png
   :alt: OptoHybrid SCA sensor circuit diagram

* Voltage  = :math:`1025 \times 0.244 \times 4 \mathrm{mV} \approx 1\mathrm{V}`

----

.. _gemos-sca-conversion-reference:

References
----------

.. [#pt100] `RTD-Pt100-Conversion.pdf <https://www.intech.co.nz/products/temperature/typert/RTD-Pt100-Conversion.pdf>`_

.. [#gbtsca] `GBT-SCA-UserManual.pdf <https://espace.cern.ch/GBT-Project/GBT-SCA/Manuals/GBT-SCA-UserManual.pdf>`_
