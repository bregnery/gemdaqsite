.. _gemos-frontend-lv-power:

====================
Low voltage powering
====================

Two LV settings are recommended depending on what equipment you have available.

.. note::
   Because the voltage at the power supply will *not* be the voltage at the terminals, especially if your cable is long.
   It is recommended to use a voltage drop compensating power supply with sense wires at the LV cable connector to the detector patch panel.

* If your power supply can go beyond 8V (e.g., A3016HP), then provide 8V at the LV terminals of the detector.
  Here the system should draw ~3A when fully configured (VFATs in sleep mode) and increase to ~3.5A when VFATs are in run mode.

* If your power supply cannot supply higher than 8V (e.g., A3016) then provide 6.5V at the LV terminals of the detector.
  Here the system will draw closer to ~5.5A when fully configured (VFATs in sleep mode) and move to ~6A when VFATs are placed in run mode.

  .. important::
     At this operating point the FEASTs will be much less efficient and may overheat easily.
     Normally this happens to the F1 and/or F2 FEASTs, which respectively supply the FPGA core voltage or VTRx/VTTx power.
     It is strongly recommended to have a fan blowing cool air over these FEASTs, and heat sinks on *all* FEASTs.

