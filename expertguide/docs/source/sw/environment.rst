.. _gemos-sw-envronment:

=================
Environment setup
=================

.. contents::
   :depth: 1
   :backlinks: entry


.. _gemos-environment-script:

Environment setup script
------------------------
From the machine you'll be setting up you have several options for setting the environment.

* This method is aggressively invasive, it applies to all users on all logins:

   .. code-block:: bash

      sudo curl -L https://cern.ch/cmsgemdaq/sw/gemos/repos/releases/legacy/gemdaqenv.sh -o /etc/profile.d/gemdaqenv.sh

* This method is better, it applies only to the specific users who enable it

  .. code-block:: bash

     curl -L https://cern.ch/cmsgemdaq/sw/gemos/repos/releases/legacy/gemdaqenv.sh -o ~/gemdaqenv.sh
     # add source ~/gemdaqenv.sh to your appropriate profile file


.. _gemos-site-variables:

Site specific variables
-----------------------

A number of shell variables should be defined for the software to work completely.
Many of these are set with the environment setup script mentioned in the previous section, but several must be set for the specific setup.

+--------------------------+----------------------------------------------------+
| Variable name            | Description                                        |
+==========================+====================================================+
| :envvar:`ELOG_PATH`      | Location the software will place certain plots     |
+--------------------------+----------------------------------------------------+
| :envvar:`DATA_PATH`      | Location the software will place output files      |
|                          | and will look for intermediate results             |
+--------------------------+----------------------------------------------------+
| :envvar:`GBT_SETTINGS`   | Location the software will look for GBTx           |
|                          | configuration settings                             |
+--------------------------+----------------------------------------------------+


.. code-block:: bash

    export ELOG_PATH=/your/favorite/elog/path
    export DATA_PATH=/your/favourite/output/path
    export GBT_SETTINGS=/your/favourite/gbt/settings/path


.. _gemos-db-connections:

DB connections
--------------

Certain tools utilize information stored in the CMS production DB.
To access this functionality from a remote teststand, a tunnel must be created to the CERN GPN, and a Oracle ``tnsnames.ora`` file created with the appropriate connection information.

.. note::
   If the ``/etc/tnsnames.ora`` file is not present on your system, you liklye haven't installed the ``cx_Oracle`` dependency.
   This package will pull in ``oracle-instantclient-basic``, which includes a default ``tnsnames.ora`` file.

.. tabs::
   
   .. tab:: OMDS (production DB)

      This will make the CMS OMDS database accessible under the name ``CMS_OMDS_TUNNEL_LOCAL``. 

      .. code-block:: bash

         ssh -L 10121:cmsonr1-v.cern.ch:10121 <username>@lxplus.cern.ch

      Then add the database descriptors to /etc/tnsnames.ora :

      .. code-block:: bash

         CMS_OMDS_TUNNEL_LOCAL=(
             DESCRIPTION=
             (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10131) )
             (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10132) )
             (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10141) )
             (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10142) )
             (LOAD_BALANCE=on)
             (ENABLE=BROKEN)
             (CONNECT_DATA=
                 (SERVER=DEDICATED)
                 (SERVICE_NAME=cms_omds_tunnel.cern.ch)
             )
         )

   .. tab:: INT2R (development DB)

      This will make the CMS INT2R database accessible under the name ``INT2R_LB_LOCAL``.

      .. code-block:: bash

         ssh \
         -L 10101:itrac1601-v.cern.ch:10121 \
         -L 10109:itrac1609-v.cern.ch:10121 \
         <username>@lxplus.cern.ch

      Then add the database descriptors to /etc/tnsnames.ora :

      .. code-block:: bash

         INT2R_LB_LOCAL = (
             DESCRIPTION =
             (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 10101))
             (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 10109))
             (LOAD_BALANCE = on)
             (ENABLE=BROKEN)
             (CONNECT_DATA =
                 (SERVER = DEDICATED)
                 (SERVICE_NAME = int2r_lb.cern.ch)
                 (FAILOVER_MODE =
                     (TYPE = SELECT)
                     (METHOD = BASIC)
                     (RETRIES = 180)
                     (DELAY = 5)
                 )
             )
         )


Subsequently, two environment variables are used by the software:

+------------------------------+----------------------------------------------------+
| Variable name                | Description                                        |
+==============================+====================================================+
| :envvar:`GEM_ONLINE_DB_NAME` | DB instanace name, in the ``tnsnames.ora`` file    |
+------------------------------+----------------------------------------------------+
| :envvar:`GEM_ONLINE_DB_CONN` | Connection name and credential, contact the        |
|                              | GEM DAQ expert team for this information           |
+------------------------------+----------------------------------------------------+


Find the Oracle RACs domain names
---------------------------------

.. important::

   If the RACs domain names change or you need to access other Oracle CERN databases, you can use the following procedure:

   * Get the database of interest descriptors in ``/etc/tnsnames.ora`` on ``lxplus``
   * Get the IP addresses of all the hosts with a DNS lookup utility (e.g. ``dig`` or ``drill``)
   * Perform a reverse DNS lookup of all the retrieved IP addresses (e.g. ``dig -x`` or ``dril -x``)
   * Establish the SSH tunnel with the RAC names where the suffix ``-s`` is replaced by ``-v`` 
