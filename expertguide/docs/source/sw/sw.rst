.. _gemos-sw-guide:

============
DAQ software
============

The GEM DAQ software is composed of several components.

.. toctree::

   install/installation
   environment
