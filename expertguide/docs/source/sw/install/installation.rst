.. _gemos-sw-installation-guide:

==================
Installation guide
==================

Installation of the GEM online software is supported by the officially provided ``yum`` packages.
Installation is also possible from source, but end users are strongly encouraged to use the official packages.
More detailed instructions are provided in the DAQ expert guide

.. rubric:: Installation methods

.. toctree::
  :maxdepth: 1

  yum
  pip
  compile


List of packages
----------------

Various packages are provided, but not all are always required:

+----------------------------+------------------------------------------------------------+
| package name               | brief description                                          |
+============================+============================================================+
| ``cmsgemos-gem*``          | ``xdaq`` related packages                                  |
+----------------------------+------------------------------------------------------------+
| ``cmsgemos_gempython``     | ``python`` bindings for hardware classes                   |
+----------------------------+------------------------------------------------------------+
| ``gempython_vfatqc``       | ``python`` tools for lab QC tests                          |
+----------------------------+------------------------------------------------------------+
| ``gempython_gemplotting*`` | ``python`` tools for analysis of lab QC tests              |
+----------------------------+------------------------------------------------------------+
| ``xhal``                   | Package providing interface between PC and back end boards |
+----------------------------+------------------------------------------------------------+
| ``ctp7_modules``           | Package providing register access functionality            |
+----------------------------+------------------------------------------------------------+


``cmsgemos``
~~~~~~~~~~~~

``xhal``
~~~~~~~~

``ctp7_modules``
~~~~~~~~~~~~~~~~

``gempython_vfatqc``
~~~~~~~~~~~~~~~~~~~~

``gempython_gemplotting``
~~~~~~~~~~~~~~~~~~~~~~~~~

