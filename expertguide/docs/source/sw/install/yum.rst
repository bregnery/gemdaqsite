.. _gemos-sw-yum-installation:

=========================
Installation with ``yum``
=========================

Obtain the repo
---------------

The GEM online software group maintains a yum repository for the officially released software packages.
To use these on your setup follow the appropriate steps below.

.. tabs::

   .. tab:: centos7

      .. code-block:: sh

         curl -L https://cern.ch/test-gemdaq-ci/sw/gemos/repos/releases/legacy/gemos_release_centos7_x86_64.repo -o /etc/yum.repos.d/gemos.repo

   .. tab:: arm

      This file should be placed on the CTP7 (eventually)

      .. code-block:: sh

         curl -L https://cern.ch/test-gemdaq-ci/sw/gemos/repos/releases/legacy/gemos_release_peta_armv7l.repo -o /etc/yum.repos.d/gemos.repo


Determine which packages you require and then install
-----------------------------------------------------
For most teststands, the following command should be sufficient

.. code-block:: sh

  yum install xhal reedmuller wiscrpcsvc gempython\* --enablerepo=cernonly

.. note::

   CERN packages and provides some packages that are not available in "standard" repositories, e.g., ``cx_Oracle``.
   In order to have ``yum`` find these packages, you should either ensure that the appropriate ``cernonly`` repository file is always enabled, or you should use the ``--enablerepo=cernonly`` flag, as shown in the example.



Install pacakges on the CTP7
----------------------------

The CTP7 requires that some setup be performed in order to be useful.
Assuming that the CTP7 is already :ref:`set up to communicate with the teststand<gemos-ctp7-setup-new>`, this simply requires putting the correct version of the libraries in the expected locations.

.. code-block:: sh

   smart install xhal reedmuller ctp7_modules
