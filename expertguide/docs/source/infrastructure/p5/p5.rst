.. _gemos-p5-infra:

====================
Infrastructure at p5
====================

The GEM infrastructure at P5 is partly managed by the CMS sysadmin team, and partly managed by the GEM DAQ group.
The sysadmins are responsible for computer hardware and ensuring software is able to be installed in an automatic (puppetized) way.
THey are also responsible for the network configuration.

The GEM DAQ experts are responsible for the GEM back-end harware (in rack ``S2E01``) and updating software repository profiles in the software dropbox.

.. contents:: Table of contents
   :local:
   :depth: 2
   :backlinks: entry

.. _gemos-sysadmin-support:

Sysadmin support
----------------

Support from the sysadmin team can be sought several ways.
The on-call CMS DAQ expert can initiate support for issues related to the central DAQ infrastructure (e.g., miniDAQ).
For all other issues, the CMS sysadmins utilize a Jira ticketing system (`described here <https://twiki.cern.ch/twiki/bin/view/CMS/Net-and-Sys-admin-Users>`_).
To open a ticket you must be a member of the appropriate egroup and then use the `CMSONS Jira portal <https://its.cern.ch/jira/browse/CMSONS>`_.


.. _gemos-p5-computers:

Computers
---------

+----------------------+---------------------------+-----------------------------+
| GEM machine alias    | rack location             | Notes                       |
+======================+===========================+=============================+
| ``bridge-s2e01-23``  | ``s2c17-22-01``           | ``control_hub`` and         |
| ``ctrl-s2c17-22-01`` |                           | ``sysmgr`` host             |
+----------------------+---------------------------+-----------------------------+
| ``gem-dqm01``        | ``s2g18-34-01``           |                             |
+----------------------+---------------------------+-----------------------------+
| ``gem-dqm01``        | ``s2g18-34-01``           |                             |
+----------------------+---------------------------+-----------------------------+
| ``gemvm-xdaq15``     | ``kvm-s3562-1-ip151-107`` |                             |
+----------------------+---------------------------+---------------------------- +
| ``gemvm-v3daq``      | ``kvm-s3562-1-ip151-74``  |                             |
+----------------------+---------------------------+-----------------------------+
| ``cmsrc-gem``        | ``kvm-s3562-1-ip151-110`` | ``RCMS`` host (``tomcats``, |
| ``cmsrc-gempro``     |                           | i.e., function managers)    |
| ``cmsrc-gemdev``     |                           |                             |
+----------------------+---------------------------+-----------------------------+
| ``cmsdropbox.cms``   | ``kvm-s3562-1-ip151-48``  | ``dropbox2`` service host   |
+----------------------+---------------------------+-----------------------------+


.. _gemos-p5-pc-ipmi:

Remote powering
~~~~~~~~~~~~~~~

Instructions on remotely powering on/off the centrally managed GEM machines can be found on the P5 cluster users guide twiki `ipmi instructions <https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#IPMI>`_.

.. important::
   These on/off actions should **only** be taken after confirmation that it is OK from the CMS sysadmins


.. _gemos-p5-dropbox:

Software dropbox
~~~~~~~~~~~~~~~~

Information on using the ``dropbox2`` tool to update the software associated with the puppetized machines can be found on the P5 cluster users guide twiki `dropbox link <https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#How_to_use_the_dropbox_computer>`_.

.. note::
   If a package has not previously been included in a GEM puppet profile, a `Jira ticket <https://its.cern.ch/jira/projects/CMSONS>`_ should be opened, providing the initial RPM.
   Following this, updating the RPM is done in the normal way with the ``dropbox2`` tool


.. _gemos-p5-infra-mon:

Monitoring (icinga)
~~~~~~~~~~~~~~~~~~~

Monitoring of central infrastructure is provided via ``icinga`` (`GPN <http://cmsicinga2.cern.ch/icinga-web/>`_) or (`P5 <http://cmsicinga2.cms/icinga-web/>`_)
SMS notifications are sent to a list of CERN mobile numbers, the DAQ expert phone is one.
Email notifications are sent to the members of the ``cms-gem-daq-notifications`` e-group.
The notification polices can be changed by opening a `Jira ticket <https://its.cern.ch/jira/projects/CMSONS>`_, using any configuration defined in `this documentation <https://docs.icinga.com/latest/en/notifications.html>`_.


.. _gemos-p5-utca-shelves:

μTCA shelves
------------

+-----------------+---------------+
| GEM μTCA shelf  | rack location |
+=================+===============+
| ``gem-shelf01`` | ``s2e01-23``  |
+-----------------+---------------+
| ``gem-shelf02`` | ``s2e01-14``  |
+-----------------+---------------+

.. tabs::

   .. tab:: Shelf 1

      * rack location: ``s2e01-23``

      +----------+--------------+-------------------------+---------------------------------------------------+
      | Slot     | S/N          | Chambers                | Notes                                             |
      +==========+==============+=========================+===================================================+
      | MCH      | XXXXX        |                         | NAT, no password                                  |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC13    | XXX          |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC1     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC2     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC3     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC4     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC5     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC6     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC7     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC8     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC9     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC10    | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC11    | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC12    | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+


   .. tab:: Shelf 2

      * rack location: ``s2e01-14``

      +----------+--------------+-------------------------+---------------------------------------------------+
      | Slot     | S/N          | Chambers                | Notes                                             |
      +==========+==============+=========================+===================================================+
      | MCH      | XXXXX        |                         | NAT, no password                                  |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC13    | XXX          |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC1     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC2     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC3     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC4     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC5     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC6     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC7     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC8     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC9     | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC10    | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC11    | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+
      | AMC12    | ``eagleXX``  |                         |                                                   |
      +----------+--------------+-------------------------+---------------------------------------------------+

Remote powering
~~~~~~~~~~~~~~~

Instructions on remotely powering on/off the μTCA shelves is not currently available.

.. important::
   These on/off actions should **only** be taken after confirmation that it is OK from the CMS TC
