.. _gemos-expert-setup:

==========
Setting up
==========

.. important::
   Setting up a machine at P5 is handled by the CMS sysadmins, and should be initiated and followed via a `Jira ticket <https://its.cern.ch/jira/projects/CMSONS>`_ (CERN SSO login required).

Setting up a new GEM DAQ teststand/lab machine should be done with the :cpp:func:`setupMachine.sh script<gemscripts:setup_machine>`.
It provides steps to go from a completely fresh machine to a fully working GEM DAQ PC with a common set of accounts and tools.

Several dependencies/requirements are worth noting:

* If the stand will operate CTP7s

  * Decide how to configure the network

    * Full support is only provided if using the UW ``sysmgr`` to manage the CTP7s

      * It provides DNS, time server, and ``rsyslog`` capabilities for the CTP7s via IPMI
      * If multiple PCs in your lab can communicate to the CTP7s, only one should be configured as the ``sysmgr`` host

    * Other configurations are possible, but support from the GEM DAQ experts will be "best effort" only and you should contact directly the UW CTP7 support team

  * Full support is only guaranteed if your μTCA setup includes an AMC13 (for clock and TTC)
  * If you have a μFEDKIT for data readout, you will need the hardware mentioned on the `uFEDKIT twiki page <https://twiki.cern.ch/twiki/bin/viewauth/CMS/UFEDKIT>`_, and the setup script configuration options will assume that it is connected properly

* The common users and groups listed below, and created when using the aforementioned script allow for

  * Scoping operational roles
  * Unified interaction with software and tooling
  * Consistent support from the DAQ expert team


Standard accounts and groups
----------------------------

Script option `U`

+---------------+-------------------------------------------------------+------+------+
| Username      | Purpose                                               | UID  | GID  |
+===============+=======================================================+======+======+
| gemuser       | Common user account for operations [deprecated]       | 5075 | 5075 |
+---------------+-------------------------------------------------------+------+------+
| gempro        | Standard production account at P5                     | 5060 | 5060 |
+---------------+-------------------------------------------------------+------+------+
| gemdev        | Special user account for non-production testing       | 5050 | 5050 |
+---------------+-------------------------------------------------------+------+------+
| gembuild      | Special account used for building patched software    | 2050 | 2050 |
+---------------+-------------------------------------------------------+------+------+
| daqpro        |                                                       | 2055 | 2055 |
+---------------+-------------------------------------------------------+------+------+

+---------------+-------------------------------------------------------+------+
| Group name    | Purpose                                               | GID  |
+===============+=======================================================+======+
| gemdaq        | DAQ expert group                                      | 2075 |
+---------------+-------------------------------------------------------+------+
| gemsudoers    | Machine sudoers group with elevated permissions       | 1075 |
+---------------+-------------------------------------------------------+------+
|               |                                                       |      |
+---------------+-------------------------------------------------------+------+


Installing software
-------------------

Script options ``xcmrpdAI``

Connecting to the NAS (**CERN only**)
-------------------------------------

Script option ``n``

Configuring network interfaces
------------------------------

Script option ``N``

Configuring 10GbE NIC for μFEDKIT
---------------------------------

Script option ``M``

Configuring sysmgr services
---------------------------

Script option ``S``

Installing Xilinx tools
-----------------------

Script options ``zvle``

