.. _gemos-expert-common-infra:

=====================
Common infrastructure
=====================

.. contents::
   :local:
   :depth: 1
   :backlinks: entry


.. _gemos-service-accounts:

Service accounts
----------------

Several service accounts have been created to aid with deployments and website serving:

+----------------------+------------------------------------------------+
| Account name         | Description                                    |
+======================+================================================+
| ``gemdaq``           |                                                |
+----------------------+------------------------------------------------+
| ``gemdaqci``         |                                                |
+----------------------+------------------------------------------------+


.. _gemos-eos-storage:

EOS storage
-----------

An EOS project space has been created to serve web pages, host software repositories, and perform various other custodial tasks.
The root of the project space is:

.. code-block::
   /eos/project/c/cmsgemdaq/


.. _gemos-websites:

Websites
--------

Currently, there are two websites serving information related to the GEM DAQ group.


cmsgemdaq
~~~~~~~~~

`https://cmsgemdaq.web.cern.ch/cmsgemdaq`_ (also accessible via `https://cern.ch/cmsgemdaq`_) is the primary entry point to the GEM DAQ information portal.
It hosts the guides, the API documentation, the ``yum`` repositories, as well as several other useful scripts.
It is served from 

.. code-block::
   /eos/project/c/cmsgemdaq/www/cmsgemdaq/


test-gemdaq-ci
~~~~~~~~~~~~~~

`https://test-gemdaq-ci.web.cern.ch/test-gemdaq-ci`_ (also accessible via `https://cern.ch/test-gemdaq-ci`_) is a temporary site, used for testing deployments.
It previously served as a backup when the primary site suffered from an outage.
It is served from 

.. code-block::
   /eos/project/c/cmsgemdaq/www/ci-test/


.. _gemos-yum-repo:

YUM repository
--------------

Software (firmware) artifacts are deployed as RPMs to an architecture dependent ``yum`` repository, hosted in the ``cmsgemdaq`` website.
The ``yum`` repository and RPM packages are signed with a GPG key (``0x5079CAAD2B12DD62``).
