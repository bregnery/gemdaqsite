.. _gemos-debugging-guide:

========================
Debugging GEM DAQ issues
========================

A number of issues can be encountered as a GEM DAQ expert.
The pages in this section will attempt to provide you with the tools required to understand how to debug
