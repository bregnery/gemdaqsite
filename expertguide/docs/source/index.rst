.. GEM DAQ Expert documentation master file, created by
   sphinx-quickstart on Thu Mar 12 15:44:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================
CMS GEM DAQ expert documentation
================================

This site complements the :std:doc:`userguide:index`, and is intended to serve as a reference for the GEM DAQ on-call experts, teststand responsibles, and admins of remote teststands.
It contains useful information on setting up a teststand from scratch, debugging software and hardware issues, and generally more detailed information than is necessary for operating the GEM DAQ system (hardware, firmware, and software).

.. toctree::
   :caption: Overview categories
   :maxdepth: 5

   system/system
   infrastructure/infrastructure
   operations/operations
   debugging/debugging

.. toctree::
   :caption: Specialized categories
   :maxdepth: 5

   sw/sw
   fw/fw
   electronics/electronics

.. toctree::
   :hidden:
   :caption: Links to other documentation
   :maxdepth: 1

   CMS GEM DAQ project page <http://0.0.0.0:8000>
   User guide <http://0.0.0.0:8000/SITE_ROOT/guides/userguide>
   Expert guide (this site) <http://0.0.0.0:8000/SITE_ROOT/guides/expertguide>
   Developer guide <http://0.0.0.0:8000/SITE_ROOT/guides/devguide>
..
   https://github.com/sphinx-doc/sphinx/issues/1836
..
   :any:`userguide:index`
   :any:`Expert guide (this site) <index>`
   :any:`devguide:index`

.. toctree::
   :hidden:
   :caption: Links to API documentation
   :maxdepth: 1

   cmsgemos <http://0.0.0.0:8000/SITE_ROOT/docs/api/cmsgemos/latest>
   gemplotting <http://0.0.0.0:8000/SITE_ROOT/docs/api/gemplotting/latest>
   vfatqc <http://0.0.0.0:8000/SITE_ROOT/docs/api/vfatqc/latest>
   ctp7_modules <http://0.0.0.0:8000/SITE_ROOT/docs/api/ctp7_modules/latest>
   reg_utils <http://0.0.0.0:8000/SITE_ROOT/docs/api/reg_utils/latest>
   xhal <http://0.0.0.0:8000/SITE_ROOT/docs/api/xhal/latest>


Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`
