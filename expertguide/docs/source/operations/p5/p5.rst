.. _gemos-expert-p5-ops-guide:

=============
P5 operations
=============

P5 operations are the primary support provided by the GEM DAQ expert team.

.. contents::
   :local:
   :depth: 1
   :backlinks: entry


.. _gemos-add-users:

Adding GEM user accounts
------------------------

Adding a new GEM user to the P5 machines is handled by the CMS sysadmin team.
The GEM responsible should open a new `Jira ticket <https://its.cern.ch/jira/projects/CMSONS>`_ reqesting the NICE username be added to the technical network, as well as the ``gempro`` group.

Adding an account to a GEM managed machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the case of machines which are not administered by the sysadmins, you can add NICE users to the machines to grant other CERN users access to the GEM machines.
This can be done via the ``setupMachine`` script, with the ``-u`` option.
You should have a text file with one entry per line (the NICE username).

.. note::
   The script will loop through all the names, so you can use it to add multiple users in one go, but it is interactive, as it also allows you to add the users to various privilegd groups and this has not yet been scripted.

Getting connected
-----------------
If you yourself do not have an account on the technical netowrk, contact the GEM responsible, or open the ticket yourself.

Once this has been done, you should have an account on the technical network.

.. note::
   The account will be created simultaneously for both the P5 (``.cms``) and 904 (``.cms904``) networks, you do not need to request an account for each.

There are several helpful snippets to include in your ``ssh`` config file:

Multi-hop
~~~~~~~~~

If connecting to the technical network from outside the CERN GPN, you will first have to hop through a machine such as ``lxplus``.
To do this easily, add the following rule to your ``~/.ssh/config`` file (or platform equipment):

.. tabs::

   .. tab:: ``.cms``
   
      .. code-block:: config
      
         ### Define a special alias to create all the necessary tunnels
         Host cmsusrtunnel
           HostName cmsusr.cern.ch
           ProxyJump lxplus.cern.ch
           DynamicForward 1091
           ### You can configure this to reuse connections
           # ControlMaster auto
           ### Local forwards for the x2go machines
           LocalForward 6122 x2go01:22
           LocalForward 6222 x2go02:22
           LocalForward 6322 x2go03:22
           LocalForward 6422 x2go04:22
           LocalForward 6522 x2go05:22
           LocalForward 6622 x2go06:22
           ### Local forwards for DB connections
           LocalForward 19121 cmsrac42-v.cms:10121
           ### Local forwards for the RCMS logging collector receivers
           # LocalForward 19021 cmsrc-srv.cms:99021
           # LocalForward 20021 cmsrc-gem.cms:20021
           # LocalForward 40021 cmsrc-gemdev.cms:40021
      
         ### Match all machines on the .cms network and route them through the special cmsusrremote alias
         Host x2go*.cms cmsnx*.cms *.cms 10.176.*.* 172.16.*.*
           ProxyJump cmsusr
      
         ### Hop through lxplus to get to the head node
         Host cmsusr
           HostName cmsusr.cern.ch
           ProxyJump lxplus.cern.ch

   .. tab:: ``.cms904``
   
      .. code-block:: config
      
         ### Define a special alias to create all the necessary tunnels
         Host cms904usrtunnel
           HostName cms904usr.cern.ch
           ProxyJump lxplus.cern.ch
           DynamicForward 1081
           ### You can configure this to reuse connections
           # ControlMaster auto
           ### Local forwards for the x2go machines
           LocalForward 7122 cms904x2go:22
      
         ### Match all machines on the .cms904 network and route them through the special cms904usr alias
         Host cms904x2go* *.cms904 10.192.*.*
           ProxyJump cms904usr
      
         ### Hop through lxplus to get to the head node
         Host cms904usr
           HostName cms904usr.cern.ch
           ProxyJump lxplus.cern.ch

   .. tab:: 904 GPN
   
      .. code-block:: config
      
         ### Define a special alias to create all the necessary tunnels
         Host gem904tunnel
           HostName lxplus.cern.ch
           DynamicForward 1085
           ### You can configure this to reuse connections
           # ControlMaster auto
           ### Set up any local forwards here

         Host gem904*.cern.ch cmssrs*.cern.c
           ProxyJump lxplus.cern.ch


   .. important::
      The above snippet uses the ``ProxyJump`` option of ``openSSH``.
      It is availble in versions greater than 7.3.
      If you are running a verision older than this, you should replace the ``ProxyJump`` statements with the appropriate option from the list below:
      * ``ProxyJump <host>`` ## OpenSSH 7.3 and up
      * ``ProxyCommand ssh <host> -W %h:%p`` ## OpenSSH 5.4 and up
      * ``ProxyCommand ssh <host> nc %h <ssh port on server>`` ## OpenSSH X.Y? and up


PAC file
~~~~~~~~

Using proxy auto-connect (PAC) file will allow you to set up tunnels such that you can easily access resources on the technical network or GPN from outside the GPN (or, if not connected to the technical network, from the GPN).
If you do this, you won't have to individually forward specific ports and then access them via ``localhost:<port>``, instead you simply point your browser to the real URL and the proxy you set up with the dynamic forward ensures that the request makes it to the correct machine.
The file `located here <https://test-gemdaq-ci.web.cern.ch/test-gemdaq-ci/guides/data/ssh/cern.pac.h>`_ will assume that you have set up the ports as in the examples above.
If not, you should ensure that the port forwards you use match the rules in the PAC file.

Enabling this in firefox is done via the preferences menu.
It can also be enabled system wide on your linux laptop, but the setup is different depending on your exact setup.
Setup on Mac can be done either through the browser or through the network settings.
Setup on Windows is an exercise left for the reader.

~~~~~~~~~

Tunneling
~~~~~~~~~

Tunneling
~~~~~~~~~

