.. _gemos-expert-xdaq-services-guide:

========================
xDAQ Services (``XaaS``)
========================


``jobcontrol``
--------------

The ``jobcontrol`` executable is able to start and stop ``xdaq.exe`` executives on demand (it is used by RCMS).
To do this, the ``jobcontrol`` service needs to be running on the machine, and in the zone to which the executive should belong.
In GEM usage, the ``jobcontrol`` process is running on port ``39999``, and its hyperdaq page is accessible there.

From the hyperdaq page, any process started by ``jobcontrol`` can be killed or its log viewed.


``xmas``
--------

``xdaq`` monitoring and alarming service, or ``xmas`` is a monoitoring and alarmaing framework provided with the xDAQ framework.
It utilizes "monitorable" items exposed through ``InfoSpace`` objects in the ``xdaq`` applications and provides a mechanism to pulse the application and request an update in the monitoring information.
It is not currently used in GEM applications.
