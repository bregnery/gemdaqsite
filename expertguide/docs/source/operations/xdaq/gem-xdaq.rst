.. _gemos-expert-gem-xdaq-guide:

========
GEM xDAQ
========

Currently, the GEM xDAQ implementation is controlled through the ``GEMSupervisor`` application.
It controls and manages various ``<X>Manager`` applications, as well as sending ``SOAP`` messages to the :ref:`TCDS<gemos-expert-tcds-guide>` applications.

Most of GEM xDAQ applications have an internal finite state machine (FSM) defining the allowable states the application may be in, as well as the allowed transitions into and out of a given state.
Transtions into each state have specific actions associate with them.
If a transition fails for one reason or another, an error message is logged and the FSM should go to the ``ERROR`` state.
