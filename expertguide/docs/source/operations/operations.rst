.. _gemos-ops-guide:

==============
DAQ operations
==============

Providing operational support is one of the primary tasks of the GEM DAQ expert team.


.. toctree::
   :caption: General information
   :maxdepth: 3

   rcms/rcms
   xdaq/xdaq
   tcds/tcds


.. toctree::
   :caption: Setup specific information
   :maxdepth: 3

   p5/p5
   904/904
