.. _gemos-expert-rcms-fm-guide:

================
Function manager
================

The GEM function manager (FM) provides the links between the GEM xDAQ applications and the CMS DAQ state machine.

