.. _gemos-expert-rs-manager-guide:

==========
RS manager
==========

The Resource Service Manager (RS Manager) is a tool provided by the CMS RCMS developers to interface with the RS database (RSDB).
This database hosts the RCMS run configurations.

.. _gemos-expert-get-rs-manager:

Obtaining the RS Manager
------------------------

Using the RS Manager
--------------------

