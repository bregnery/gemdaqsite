.. _gemos-expert-rcms-guide:

====
RCMS
====

Run Control and Monitoring System (RCMS) is the interface between the xDAQ code and central operations.


.. toctree::
   :maxdepth: 2

   fm
   minidaq
   rs-manager
