.. _gemos-expert-minidaq-guide:

=======
miniDAQ
=======

In addition to the standard RCMS FM interface, a special set of applications and infrastructure is provided by the cDAQ group to mimic as closely as possible, cDAQ operations.
This infrastructure is known as "miniDAQ".
The GEM miniDAQ configuration by default contains a DAQ function manager and a TCDS function manager.


.. contents:: Table of contents
   :local:
   :depth: 2
   :backlinks: entry


.. _gemos-minidaq-automator:

Using the automator
-------------------

The "automator" configuration allows you to create a run and get to any state provided all intermediate states transitioned successfully.


.. _gemos-minidaq-add-remove-fed:

Add/remove FED
--------------

To add or remove a FED from the run, use the ``FED&TTS`` button.
Here you must select the FED, whether the DAQ is enabled/disabled, and whether the TTS is enabled/disabled.


.. _gemos-minidaq-random-trigger:

Set random trigger rate
-----------------------

Changing the random trigger rate is done in the ``LPMController`` :ref:`application<gemos-tcds-lpm-controller>`.


.. _gemos-minidaq-hlt-key:

Select CMSSW release (HLT key)
------------------------------

Use the ``HLT`` selector to choose the HLT menu as well as the appropriate CMSSW release to be used.


.. _gemos-minidaq-t0-transfer:

Enable/disable T0 transfer
--------------------------

If the data being taken should be written out to the T0, make sure to set the ``T0_TRANSFER_ON``


.. _gemos-minidaq-stress-test:

MiniDAQ stress-test
-------------------

One of the main uses of miniDAQ is to emulate the conditions used during cDAQ operations when validating new software and firmware.
Setting up for a "stress-test" involves configuring a run.
Several subsequent steps would involve:
* cycling through many start/stop transitions
* setting a high random trigger rate (>100kHz)
* enabling standard CMS B-go sequences (``HardReset``, ``Resync``)
