.. _gemos-mch-guide:

===
MCH
===

In the μTCA world, the μTCA shelf has a carrier hub, which connects it to the host network, and also provides a network switch to the cards in the shelf.
It also functions as a shelf manager and provides power managment for the cards in the shelf.
