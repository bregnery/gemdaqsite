.. _gemos-ctp7-linux-image:

====================
The CTP7 Linux image
====================

.. warning::
   If you are not a DAQ expert, you should not executing procedures listed on this page!


.. _gemos-update-linux-image:

Updating the CTP7 Linux image
-----------------------------

#. Login as ``root`` (this logs in at ``/home/root`` which is not on the ``/mnt`` partition),
#. Execute ``/sbin/reboot`` to make sure there are no running processes or other active sessions (this would prevent a Linux update),
#. Login as ``root`` again and mount the partitions as ``rw``:

   .. code-block:: bash

      mount -o remount,rw /dev/mmcblk0p3 /mnt/persistent
      mount -o remount,rw /dev/mmcblk0p1 /mnt/image
      mount -o remount,rw /dev/mmcblk0p2 /mnt/image-persist

#. Place the Linux image in the ``root`` ``$HOME`` directory (so that it is *not* on the ``/mnt`` partition),
#. As ``root`` execute ``image-update linuxImageFile.img``, an example *successful* output would look like

   .. code-block:: bash

      root@eagle26:~# image-update LinuxImage-CTP7-GENERIC-20180529T153916-0500-4935611.img
      Ensuring /mnt/persistent is writeable.
      *** Extracting Image ***

      *** Verifying Signature ***
      Verified OK

      *** Extracting Image Contents ***

      *** Running Installation Script ***

      Installing Image: CTP7-GENERIC-20180529T153916-0500-4935611
      Mounting image filesystem read-write
      Copying boot image
      Mounting image filesystem read-only
      Mounting image-persist filesystem read-write
      Installing stage tarball
      Installing documentation
      Syncing

      Update complete!
      Rebooting!

      Broadcast message from root@eagle26 (pts/0) (Thu Aug  9 08:31:00 2018):

      The system is going down for reboot NOW!

#. Finally, login again (as any user) and check that the `build_id` reflects the new image, for the above example it would have printed:

   .. code-block:: bash

      eagle26:~$ cat /mnt/image-persist/build_id
      CTP7-GENERIC-20180529T153916-0500-4935611


.. _gemos-filesystem-readonly:

Filesystem mounted as readonly
------------------------------

More recent Linux images of the CTP7 have mounted ``/mnt/persistent/`` partition as ``readonly``.
This can be resolved by doing

.. code-block:: bash

   echo yes > /mnt/persistent/config/persistent_writeable

The next time the card boots the ``/mnt/persistent`` partition will be mounted as writeable.

If the system is running and cannot be rebooted (e.g., during data-taking) the following short cuts exist and can be executed by non-``root`` users:

.. code-block:: bash

   setpersistent rw # Sets partition writeable
   setpersistent ro # Sets partition readonly

Some older versions of the Linux image do *not* feature these commands, in this case, mount the partitions as ``rw`` via:

.. code-block:: bash

   mount -o remount,rw /dev/mmcblk0p3 /mnt/persistent
   mount -o remount,rw /dev/mmcblk0p1 /mnt/image
   mount -o remount,rw /dev/mmcblk0p2 /mnt/image-persist


.. _gemos-ctp7-syslogd:

Logging (``syslogd``)
---------------------

The system log of the CTP7 can be found here:

.. code-block:: bash

   /var/log/messages

.. note::
   This file is *not* found under the ``/mnt/persistent`` partition and is re-written everytime the card reboots.
   To ensure log information is not lost, the DAQ PC hosting the ``sysmgr`` is configured to receive the same log messages remotely.
   On that PC the log file is file found under:

   .. code-block:: bash

      /var/log/remote/<cardname>/messages.log

   Where ``<cardname>`` is the IP address or network alias of the CTP7 (e.g., ``eagle64``), and there will typically be two directories for each CTP7.
   This file is written to disk on the DAQ PC and will persist through crashes/reboots of the CTP7 (so no information will be lost).

You can see the most recent information found in the log file via:

.. code-block:: bash

   tail -25 /var/log/messages

This will display the last 25 lines in the log file on the CTP7 (executed as ``gemuser`` on the CTP7).
Use the ``tail`` command similarly to view the log on the DAQ PC.


.. _gemos-restart-syslogd:

Restarting ``syslogd``
~~~~~~~~~~~~~~~~~~~~~~

In some weird cases ``syslogd`` may not be running after the CTP7 boots.
This typically indicates that the CTP7 did not recieve a full configuration from the ``sysmgr``.
The result is that the log file:

.. code-block:: bash

   /var/log/messages

will not being created.
To resolve this, on the CTP7  execute (replace ``192.168.0.180`` with the IP address of the DAQ PC on which the ``sysmgr`` service is running):

.. code-block:: bash

   /sbin/syslogd -R 192.168.0.180 -L -s 1024 -b 8

and the process should again be running, e.g.,

.. code-block:: bash

   root@eagle63:~# ps l | grep syslog
   S     0  9161     1  2792    64 0:0   13:31 00:00:00 /sbin/syslogd -R
   192.168.0.180 -L -s 1024 -b 8
   S     0  9163  9146  2796   288 pts4  13:32 00:00:00 grep syslog
