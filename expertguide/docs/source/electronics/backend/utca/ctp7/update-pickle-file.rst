.. _gemos-ctp7-update-pickle:

====================================
Update the address table pickle file
====================================

For certain tools on the CTP7, the LMDB is not used to look up the register to address mapping, and instead this is done with a "pickled" version of the XML address table.
When changing the firmware, in addition to updating the XML address tables and the LMDB, the ``pickle`` file itself must also be updated, in order to ensure consistency between the three sources of information.

.. note::
   In the future, this will no longer be necessary, as there will be one single source of truth


All steps except for the final step are performed whenever the ``setup_ctp7.sh`` script is provided with *either* the ``-o`` or ``-c`` options, indicating that firmware for either the OptoHybrid or CTP7 should be updated (see `:ref:gemos-ctp7-helper-scripts`).
When using that script, a message will be printed 

.. code-block:: bash

   New pickle file has been copied to the CTP7, make sure you modify it correctly

which means that you should manually modify the pickle file *in situ* on the CTP7.


The full procedure
------------------

#. On the DAQ machine delete the pickle file found under ``$GEM_ADDRESS_TABLE_PATH``

   .. code-block:: bash
                   
      rm ${GEM_ADDRESS_TABLE_PATH}/amc_address_table_top.pickle

#. Create a new pickle file on the DAQ machine, to do this execute: ``gem_reg.py``, this will automatically create a new pickle file under ``$GEM_ADDRESS_TABLE_PATH``.
   Then exit the tool by typing ``exit``.

#. Upload the new pickle file to the CTP7:

   .. code-block:: bash
                   
      scp ${GEM_ADDRESS_TABLE_PATH}/amc_address_table_top.pickle root@eagleXX:/mnt/persistent/gemdaq/xml/gem_amc_CTP7FW.3.A.B_OHFW3.X.Y.Z.pickle

   where 3.A.B is the CTP7 FW version and 3.X.Y.Z is the OH FW version.

#. As ``root`` on the CTP7

   #. Navigate to the xml address folder
   
    .. code-block:: bash
                    
       cd /mnt/persistent/gemdaq/xml
   
   #. Open the pickle file with a text editor
   
    .. code-block:: bash
                    
       vi gem_amc_CTP7FW.3.A.B_OHFW3.X.Y.{A|B|C}.pickle
   
   #. Replace the third line of the pickle file:
   
      .. code-block:: bash
   
         q^A]q^B(]q^C(U^GGEM_AMCq^D(creg_utils.reg_interface.common.reg_xml_parser
   
      to match:
   
      .. code-block:: bash
   
         q^A]q^B(]q^C(U^GGEM_AMCq^D(crw_reg
   
      .. warning::
         **Do not copy paste this, you *must* manually type it**.
         If you copy/paste you may insert a hidden control character that will cause the file to **not** be parsed correctly and any subsequent register access action will fail.

