.. _gemos-ctp7-guide:

====
CTP7
====

For a generic overview of the CTP7 see :ref:`userguide:gemos-ctp7-guide`.
In the pages linked below, you will find various expert level documentation to set up and configure the CTP7, as well as various hints to debug and resolve various issues.

.. toctree::
   :caption: Overview
   :maxdepth: 1

   overview

.. toctree::
   :caption: Common operations
   :maxdepth: 2

   rpcsvc
   gem-reg
   reprogramming
   update-pickle-file
   update-oh-fw
   helper-scripts
   linux-image
