.. _gemos-ctp7-reprogramming:

===========================
Reprogramming the CTP7 FPGA
===========================

All current generation electronics systems use AMC firmware version ``3.X.Y`` or higher.

.. important::
   This procedure should **always** be done as ``gemuser``


.. _gemos-ctp7-reload-fw:

FW reload (``cold_boot``)
-------------------------

To only reload the firmware (or load a newly installed firmware image), a reduced ``cold_boot`` procedure can be followed.
Depending on the version of the hardware, the polarity of some of the links in the CXPs  may be inverted, thus choose the appropriate action based on your setup:

.. tabs::

   .. tab:: GE1/1 v3

      .. code-block:: bash

         cold_boot_invert_tx.sh

   .. tab:: GE2/1

      .. code-block:: bash

         cold_boot_invert_cxp_rx.sh

   .. tab:: ME0

      .. code-block:: bash

         cold_boot_invert_cxp_rx.sh

   .. tab:: GE1/1 v2b (**OBSOLETE**)

      The v2b hardware is considered end-of-life and legacy system.
      Little to no support is available for this hardware.
      If your CTP7 is connected to a v2b hardware then you'll need to use:

      .. code-block:: bash

         cold_boot.sh

.. important::

   It is crucial to ensure that all ``GTH Status`` values (0 through 35) return ``0x7``.
   If ``0x6`` is returned then you'll need to repeat the ``cold_boot`` procedure.
   If any other value is returned (e.g., ``0x0``) the CTP7 may not be receiving a clock from the ``AMC13`` and you'll need to check that the AMC13 is configured correctly (see :ref:`userguide:gemos-amc13-enabling-clock`).


.. _gemos-ctp7-recovery:

Full recovery: ``recover.sh``
-----------------------------

If you need to perform a full recovery, e.g., after a reboot of the CTP7 or a power cut, simply execute:

.. code-block:: bash

   recover.sh

This script lives in the ``/mnt/persistent/gemdaq/scripts`` directory, and upon execution will:

* Reload the CTP7 FW
* Start ``ipbus``
* Start ``rpcsvc``
* Load the OH FW into the CTP7 RAM for PROM-less, i.e., ``BLASTER(tm)``, programming
* Disable forwarding of TTC ``HardReset`` signals to the front-end

Again, it is critical to ensure that all ``GTH Status`` values (0 through 35) return ``0x7``.

.. warning::
   Sometimes this will not enable ``rpcsvc`` or ``ipbus`` correctly.
   After calling ``recover.sh`` it is important to check if ``rpcsvc`` and ``ipbus`` are running on the card.

An example of a successful recovery is illustrated in this `elog entry <http://cmsonline.cern.ch/cms-elog/1060543>`_.

.. note::
   If you are calling this after the card has been rebooted or power cycled you should ensure the ``texas`` account is not the owner of the ``rpcsvc`` service.
   You should, as either ``root`` or ``texas``, issue ``killall rpcsvc``, and then logout and log back in as ``gemuser`` to issue the recover command.
