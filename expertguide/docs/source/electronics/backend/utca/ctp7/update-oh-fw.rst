.. _gemos-ctp7-update-optohybrid-fw:

========================
Update the OptoHybrid FW
========================

The bitfile for the OptoHybrid FPGA is loaded into the RAM on the CTP7, and from there it is loaded remotely to the connected OptoHybrids.

.. important::
   The following requires knowledge of the ``root`` password of the CTP7.
   If you do not know the ``root`` password you should not be executing this procedure.
   Please contact the sysadmin of your test stand and ask to have this done for you.


Most of the following steps are performed whenever the ``setup_ctp7.sh`` script is provided with the ``-o`` option, indicating that firmware for the OptoHybrid should be updated  (see `:ref:gemos-ctp7-helper-scripts`).
The script will **not** create an elog for you, nor will it automatically load the updated OptoHybrid firmware into the CTP7 RAM or reprogram the connected OptoHybrids.

The full procedure
------------------

#. Begin by creating an elog entry in the relevant elog explaining what you are about to do (e.g., for 904 Coffin or Integration stand use the ``904 Integration`` elog, for QC8/V3 Electronics R&D use the ``Quality Control/DAQ Station`` elog, for P5 use the ``P5/DAQ`` elog.)
#. Login to the DAQ machine of interest (e.g., ``gem904qc8daq`` or ``gem904daq01``)
#. Navigate to the `OptoHybridv3 FW repository release page <https://github.com/cms-gem-daq-project/OptoHybridv3/releases>`_
#. Select the release you are interested in

   .. note::

      GE1/1 OptoHybrid FW are labeled as ``3.X.Y.(A|B|C|1C)``
      Very few ``OHv3a`` and ``OHv3b`` pieces still remain in use, so current firmware is only produced for long (``1C``) and short (``C``) GEB compatibility.
      GE2/1 OptoHybrid FW are labeled as ``3.X.Y.2A`` (yes, it is inconsistent, but they are also called OHv1 and OHv2...)

   For the purpose of this description, we will assume a version of ``3.2.8.1C``.

#. Navigate to the firmware directory on the 904 NAS for OH FW:

   .. code-block:: bash
                   
      ## GE1/1
      cd /data/bigdisk/GEMDAQ_Documentation/system/firmware/files/OptoHybrid/V3/OHv3(a|b|c)_firmware
      ## GE2/1
      cd /data/bigdisk/GEMDAQ_Documentation/system/firmware/files/OptoHybrid/GE2.1_Artix7

#. Download the ``OH_3.2.8.1C.tar.gz`` file from the release to the NAS area on 904, do this via:

   .. code-block:: bash

      curl -LO  https://github.com/cms-gem-daq-project/OptoHybridv3/releases/download/3.2.8.X/OH_3.2.8.1C.tar.gz

#. Unpack the archive by executing (this will create a subfolder ``OH_3.2.8.1C``):

   .. code-block:: bash

      tar zxf OH_3.2.8.1C.tar.gz

#. Navigate to the newly created subfolder:

   .. code-block:: bash

      cd OH_3.2.8.1C

#. Create ``*.bit`` and ``*.mcs`` file symlinks:

   .. code-block:: bash
                   
      ln -sf OH_3.2.8.1C.bit optohybrid_top.bit
      ln -sf OH_3.2.8.1C.mcs optohybrid_top.mcs

#. Upload the ``*.bit`` and  ``*.mcs`` files and symlinks to the CTP7 (where XX is the serial number of the CTP7):

   .. code-block:: bash

      rsync -ach --progress --partial --links OH_*.{bit,mcs} root@eagleXX:/mnt/persistent/gemdaq/oh_fw

#. Create the ``optohybrid_registers.xml`` symlink:

   .. code-block:: bash
                   
      ln -sf oh_registers_3.X.Y.xml optohybrid_registers.xml

#. Upload the the xml address table and symlink to the CTP7:

   .. code-block:: bash

      rsync -ach --progress --partial --links *.xml root@eagleXX:/mnt/persistent/gemdaq/xml

#. Login to the CTP7 of interest and load the new OH FW into the CTP7 RAM:

   .. code-block:: bash
                   
      ssh gemuser@eagleXX
      cd /mnt/persistent/gemdaq/gemloader
      ./gemloader_configure.sh
   
#. Update the ``optohybrid_registers.xml`` symlink on the DAQ machine:

   .. code-block:: bash

      ln -sf /data/bigdisk/GEMDAQ_Documentation/system/firmware/files/OptoHybrid/V3/OHv3c_firmware/OH_3.2.8.1C/oh_registers_3.2.8.1C.xml ${GEM_ADDRESS_TABLE_PATH}/optohybrid_registers.xml

#. Make sure the symlink you just created is valid
#. Update the ``pickle`` file following :ref:`these instructions<gemos-ctp7-update-pickle>`
#. If the address table has changed you must update the ``LMDB`` on the CTP7, see :ref:`gemos-ctp7-update-lmdb`.
#. To confirm that the update was successful, reprogram all OptoHybrids following the instructions under :ref:`gemos-optohybrid-programming`.

   .. warning::
      This will kill any running process on the hardware, but if you're updating FW no one should be using the system anyway.

#. Summarize the actions you took in the elog entry you have already started.
