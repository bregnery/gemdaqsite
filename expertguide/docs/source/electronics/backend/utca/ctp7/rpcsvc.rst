.. _gemos-ctp7-rpcsvc:

======================
The ``rpcsvc`` service
======================

Many actions on the card requie the Remote Procedure Call (RPC) service to be running and owned by the ``gemuser`` account.
This service is run as a system process called ``rpcsvc``.
To check if this process is running login to the CTP7 and execute:

.. code-block:: bash

   ps | grep [r]pcsvc

Example output should look like:

.. code-block:: bash

   10994 gemuser   4040 S    rpcsvc

If you only see a line that says ``grep rpcsvc`` then the ``rpcsvc`` service is *not* running.
Additionally if you see that ``rpcsvc`` is running but it is not owned by the ``gemuser`` account then it will be configured with the *wrong* library and must be killed (either as the ``root`` user or as ``$USER`` owning the process).

.. note::
   You might find that multiple lines are returned which show multiple ``rpcsvc`` services.
   If these are all owned by ``gemuser``, this simply indicates that the ``rpcsvc`` service is running and that one or more open ``rpc`` connections exist between the card and the DAQ PC.

Sometimes you may need to restart the ``rpcsvc`` (typically after updating the ``rpcmodules`` on the card), to do this execute:

.. code-block:: bash

   killall rpcsvc && rpcsvc


.. _gemos-ctp7-lmdb:

RPC modules and the LMDB
------------------------

When a command to interface with the front-end hardware is sent from the DAQ machine this typically uses either the μTCA hardware access library (``uhal``) built on top of ``ipbus``, or the cross hardware access library (``xhal``) built on top of ``rpcsvc`` (and using ``TCP/IP`` communication).
In the second case, an RPC message is sent to the CTP7 and the Zynq processor on the CTP7 will load a function that is defined in a shared object library (``*.so``) file under ``/mnt/persistent/rpcmodules``.
These shared object libraries are referred to as ``rpcmodules``.
For more detailes please see the `README of the ctp7_mopdules <https://github.com/cms-gem-daq-project/ctp7_modules/blob/develop/README.md>`_ repository.

On a production machine these will be the following set (although more may be present in the future):

.. code-block:: bash

   -rwxr-xr-x    1 root    root        49093 Oct  9 16:48 amc.so
   -rwxr-xr-x    1 root    root       140180 Oct  9 16:48 calibration_routines.so
   -rwxr-xr-x    1 root    root        95769 Oct  9 16:48 daq_monitor.so
   -rwxr-xr-x    1 root    root        13917 Oct  9 16:48 extras.so
   -rwxr-xr-x    1 root     root        102207 Sep 13  2017 memory.so
   -rwxr-xr-x    1 root     root        122365 Sep 13  2017 optical.so
   -rwxr-xr-x    1 root    root       102929 Oct  9 16:48 optohybrid.so
   -rwxr-xr-x    1 root     root        108849 Sep 13  2017 rpctest.so
   -rwxr-xr-x    1 root    root        97422 Oct  9 16:48 utils.so
   -rwxr-xr-x    1 root    root        84989 Oct  9 16:48 vfat3.so

While on a development system, these **may** be a set of symlinks that point to a user editable area, for example ``eagle64``:

.. code-block:: bash

   lrwxrwxrwx    1 root     root            47 Oct  1 12:15 amc.so -> /mnt/persistent/gemuser/rpcmoduleTesting/amc.so
   lrwxrwxrwx    1 root     root            64 Oct  1 12:15 calibration_routines.so -> /mnt/persistent/gemuser/rpcmoduleTesting/calibration_routines.so
   lrwxrwxrwx    1 root     root            55 Oct  3 12:09 daq_monitor.so -> /mnt/persistent/gemuser/rpcmoduleTesting/daq_monitor.so
   lrwxrwxrwx    1 root     root            50 Oct  1 12:15 extras.so -> /mnt/persistent/gemuser/rpcmoduleTesting/extras.so
   -rwxr-xr-x    1 root     root        102207 Sep 13  2017 memory.so
   -rw-r--r--    1 root     root        611328 Jun 27 09:07 modules.tar
   -rwxr-xr-x    1 root     root        122365 Sep 13  2017 optical.so
   lrwxrwxrwx    1 root     root            54 Oct  1 12:16 optohybrid.so -> /mnt/persistent/gemuser/rpcmoduleTesting/optohybrid.so
   -rwxr-xr-x    1 root     root        108849 Sep 13  2017 rpctest.so
   lrwxrwxrwx    1 root     root            49 Oct  1 12:16 utils.so -> /mnt/persistent/gemuser/rpcmoduleTesting/utils.so
   lrwxrwxrwx    1 root     root            49 Oct  1 12:16 vfat3.so -> /mnt/persistent/gemuser/rpcmoduleTesting/vfat3.so

where the user editable area is: ``/mnt/persistent/gemuser/rpcmoduleTesting/``.

The LMDB is an in-memory key-value-store used by the ``rpcmodules`` to map register names to addresses.
The ``rpcmodules`` are segmented into what are referred to as local or remote/non-local modules.
The local version has ``someStringLocal()`` always in it's function name and is only ever executed from the CTP7.
The non-local module receives the RPC Message from the host DAQ PC, calls the corresponding local function(s) and returns and RPC response to the host machine.
Note that no terminal output appears on the host machine during the execution of either the local or non-local ``rpcmodules``.
Instead the developer will have configured the functions to log actions on the CTP7's log file which is available on the CTP7 at:

