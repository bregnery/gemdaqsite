.. _gemos-ctp7-overview:

======================
In-depth CTP7 overview
======================

The CTP7 runs a special 32-bit version of linux located on a 32GB flash SD card inserted on the AMC.
This is run by a 32-bit processor called a Zynq processor and features extremely fast register access to the Virtex 7 FPGA.
The linux OS is loaded from an image on the card at each boot/reboot and only those files found under:

.. code-block:: bash

   /mnt/persistent

are persistent across reboots.

.. important::
   An important caveat is that the ``root`` password of the CTP7 is the same on *all* CTP7's.
   If you are the sysadmin of a given test stand *do not* share this with general users, as this will enable them to have ``root`` privileges on any test stand (including P5).


Each CTP7 has an associated, hard-coded, "birdname", e.g., ``eagle64``.
In addition, when configured to receive a configuration from the UW ``sysmger``, each card will receive a second IP address via DHCP.
This second IP address is given the geographic hostname, e.g., ``gem-shelfXX-amcYY``

.. _gemos-uw-sysmgr:

UW ``sysmgr``
-------------

The UW system manager (``sysmgr``) is an application running on a DAQ PC that communicates directly with the CTP7 via IPMI.
It provides DNS, time server, remote logging, and DHCP between the PC and any managed cards.
Configuration of the PC to serve as the ``sysmgr`` host can be done via the :ref:`setupMachine.sh script<cmsgemos:setup-machine-script>`.

.. _gemos-ctp7-filesystem:

CTP7 filesystem
---------------

On the CTP7 there will be several important directories:

.. code-block:: bash

   /mnt/image-persist
   /mnt/persistent/config
   /mnt/persistent/gemdaq
   /mnt/persistent/gemuser
   /mnt/persistent/rpcmodules

Most are described below, but the ``rpcmodules`` subdirectory is described in :ref:`gemos-ctp7-lmdb`.


.. _gemos-ctp7-image-directory:

``/mnt/image-persist``
^^^^^^^^^^^^^^^^^^^^^^

This directory contains static information related to the Linux image itself, e.g., if you want to know the build ID of the booted Linux image:

.. code-block:: bash

   $ cat /mnt/image-persist/build_id
   CTP7-GENERIC-20181120T115721-0600-0cdfd2c

Here you will also find the tarball of the associated Linux "Peta stage", which is used when compiling libraries and executables to run directly on the CTP7.


.. _gemos-ctp7-config-directory:

``/mnt/persistent/config``
^^^^^^^^^^^^^^^^^^^^^^^^^^

The files in this directory allow you to modify certain configuration parameters of the Linux image to persist across reboots.
Namely, here you will be able to ensure that newly created users/groups will persist,

* ``root.authorized_keys``

  Adding an ``ssh`` public key here will enable passwordless login as the ``root`` user.

* ``rpcsvc.acl``

  Add subnet domains here to allow RPC connections from the necessary machines.
  If you run the CTP7s on a ``192.168.0.0/16`` subnet, you may have the following entries:

  .. code-block:: bash

     $ cat /mnt/persistent/config/rpcsvc.acl

  which should produce output:

  .. code-block:: bash

     # This file contains the list of IP addresses permitted to access rpcsvc.
     # Each line contains an IP address in CIDR format.
     # Comments begin with # at any point.
     #
     # If you aren't familiar with CIDR, use /32 as the prefix length.
     #
     # Don't remove 127.0.0.1.
     127.0.0.1/32 # Local clients
     192.168.0.0/16 # Lab private networks

  You should configure this to lock down the access if the network you operate the CTP7s on is not completely under your control.

.. _gemos-ctp7-gemdaq-directory:

``/mnt/persistent/gemdaq``
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``gemdaq`` subdirectory looks like:

.. code-block:: bash

   drwxrwxrwx    2 root     root          4096 Aug 10 12:44 address_table.mdb
   drwxr-sr-x    5 gemuser  1001          4096 Jun  1  2017 apps
   drwxr-xr-x    2 51446    1399          4096 Aug  8 15:45 bin
   drwxr-xr-x    2 51446    1399          4096 Aug  8 15:45 fw
   drwxr-xr-x    5 root     root          4096 Aug 10 09:24 gbt
   drwxrwxr-x    2 51446    1399          4096 Aug  8 15:45 gemloader
   drwxr-xr-x    2 51446    1399          4096 Aug  8 15:45 lib
   drwxr-xr-x    2 51446    1399          4096 Aug  8 15:41 oh_fw
   drwxr-xr-x    3 51446    1399          4096 Aug  8 15:45 python
   drwxr-xr-x    3 51446    1399          4096 Aug  8 15:41 scripts
   drwxrwxrwx    2 51446    1399         12288 Aug 21 14:01 vfat3
   drwxr-xr-x    2 51446    1399          4096 Aug  8 15:46 xml

The contents of the most relevant of these subdirectories is described in more detail below.

* ``address_table.mdb``

  The Lightning in Memory Database (LMDB) will be found under the ``address_table.mdb`` folder, along with a lock file to prevent simultaneous access.

  .. note::
     The ``address_table.mdb`` folder and its contents must have read/write permissions to *everyone* or else LMDB related actions *will* fail.

* ``fw``

  The CTP7 firmware will be found under ``fw`` folder and a set of symlinks will be specified there, for example:

  .. code-block:: bash

     lrwxrwxrwx    1 51446    1399            23 Aug  8 15:45 gem_ctp7.bit -> gem_ctp7_v3_5_3_4oh.bit
     -rw-r--r--    1 51446    1399      28734919 Aug  3 17:37 gem_ctp7_v3_5_3_4oh.bit

* ``gbt``

  The ``GBTx`` configuration files for programming over the fiber link will be found under the ``gbt`` folder and in relevant subfolders:

  .. code-block:: bash

     drwxr-xr-x    2 root     root          4096 Aug 10 09:24 OHv3a
     drwxr-xr-x    2 root     root          4096 Aug 10 09:24 OHv3b
     drwxr-xr-x    2 root     root          4096 Aug 10 14:31 OHv3c

* ``gemloader``

  Configuring the ``gemloader`` for ``BLASTER(tm)`` configuration method is possible with the ``gemloader_configure.sh`` script, which is found under the ``gemloader`` subdirectory.
  The ``gemloader`` itself is a system installed executable, e.g.:

  .. code-block:: bash

     which gemloader

  which should produce output:

  .. code-block:: bash

     /bin/gemloader

* ``lib``

  The ``lib`` folder has a set of shared object libraries installed that are necessary for atomic transactions and logging.

* ``oh_fw``

  The OptoHybrid firmware will be found under ``oh_fw`` and a set of symlinks will be specified there, for example:

  .. code-block:: bash

     lrwxrwxrwx    1 51446    1399            22 Aug  8 15:41 optohybrid_top.bit -> optohybrid_3.1.2.B.bit
     lrwxrwxrwx    1 51446    1399            22 Aug  8 15:41 optohybrid_top.mcs -> optohybrid_3.1.2.B.mcs
     -rwxr-xr-x    1 gemuser  1001       5465091 Jun  1  2017 optohybrid_top_2.2.d.fb.bit
     -rwxr-xr-x    1 gemuser  1001      15030033 Jun  1  2017 optohybrid_top_2.2.d.fb.mcs

* ``python``

  The ``python`` folder contains several register interface scripts specifically the ``gbt.py``, ``sca.py``, and ``reg_interface.py`` scripts that can be used on the CTP7.
  Except for ``gbt.py`` these scripts are typically used from the corresponding versions on the DAQ PC.

* ``scripts``

  The ``scripts`` directory has a series of scripts that are in the ``$PATH`` that enable actions like reloading the CTP7 firmware or starting ``ipbus`` (just to name a few examples).

* ``vfat3``

  The ``vfat3`` directory for the time being contains the per ``(ohN,vfatN)`` configuration file specifies registers per chip (e.g., here you would edit the ``CFG_IREF`` for the VFAT of interest).
  Within the ``vfat3`` directory there will be a set of configuration files and symlinks that are given by the following pattern:

  .. code-block:: bash

     lrwxrwxrwx    1 gemuser  1001            53 Aug 10 15:27 config_OHX_VFATY.txt -> /mnt/persistent/gemdaq/vfat3/config_OHX_VFATY_cal.txt
     -rw-r--r--    1 gemuser  1001          1267 Aug 10 15:27 config_OHX_VFATY_cal.txt

     The symlink is what is used by the configuration command to configure the ``vfat3`` in ``(ohN,vfatN) = (X,Y)`` position; so this must always be a valid link.
     An example of how this file is expected to look can be found in the section :ref:`gemos-frontend-vfat3-config-file-ctp7`.

* ``xml``

  The address table ``xml`` files will be found under the ``xml`` folder and a set of symlinks will be specified there, for example:

  .. code-block:: bash

     -rw-r--r--    1 51446    1399      21149299 Aug  8 15:46 gem_amc_top.pickle
     lrwxrwxrwx    1 51446    1399            18 Aug  8 15:45 gem_amc_top.xml -> gem_amc_v3_5_3.xml
     -rw-r--r--    1 51446    1399        136064 Aug  8 15:45 gem_amc_v3_5_3.xml
     -rw-r--r--    1 51446    1399        102472 Aug  8 15:41 oh_registers_3.1.2.B.xml
     lrwxrwxrwx    1 51446    1399            24 Aug  8 15:41 optohybrid_registers.xml -> oh_registers_3.1.2.B.xml


.. _gemos-ctp7-gemuser-directory:

``/mnt/persistent/gemuser``
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``gemuser`` subdirectory is the ``$HOME`` folder of the ``gemuser`` user.
Here you will be able to modify the environment that is set at login, as well as add ``ssh`` keys for passwordless login as the ``gemuser`` user.
