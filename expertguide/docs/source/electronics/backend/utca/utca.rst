.. _gemos-utca-guide:

====
μTCA
====

The GEM back-end electronics are hosted in a μTCA shelf.
The shelf and its common components provide power, clock, and networking to the cards in the shelf.

.. toctree::
   :caption: μTCA system
   :maxdepth: 5

   aspiro-power-one
   mch
   amc13/amc13
   ctp7/ctp7
