.. _gemos-backend-guide:

====================
Back-end electronics
====================

The GEM back-end electronics system is composed of custom μTCA (and ATCA) FPGA processor cards.
The processor cards are responsible for slow-control, translating trigger and timing signals coming from the central system, building events, sending the events to the central DAQ system, and sending trigger stubs to the EMTF for inclusion in the muon trigger system of CMS.
Information is given in the linked documents on interacting with the different components, as well as how to deal with pre and post power cut situations

.. toctree::
   :caption: Back-end electronics
   :maxdepth: 5

   utca/utca
   atca/atca
