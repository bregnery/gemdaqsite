.. _gemos-frontend-guide:

=====================
Front-end electronics
=====================

Front-end electronics are those present on-detector.
The primary component is the :ref:`VFAT3<gemos-vfat3-guide>` chip, which translates the detector readout signals into hit information.
The :ref:`OptoHybrid<gemos-optohybrid-guide>` links the back-end electronics with the detector via custom designed, radiation hard, optical links (VTTx/VTRx) and custom radiation hard ASICs (:ref:`GBTx<gemos-gbtx-guide>` and :ref:`SCA<gemos-sca-guide>`).
Power is distributed to the on-detector components via a custom DC-DC converter, the :ref:`FEASTMP<gemos-feast-guide>`.


.. toctree::
   :caption: Front-end electronics
   :maxdepth: 2

   optohybrid/optohybrid
   vfat/vfat3
