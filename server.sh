#!/bin/sh

if [ $1 = "start" ]
then
    ps -p `cat server/server.pid` >& /dev/null
    if ! [ "$?" = "0" ]
    then
        echo "Starting local server"
        python3 -m http.server --directory server >& server/server.log &
        echo $! > server/server.pid
    else
    echo "Local server already running, PID" `cat server/server.pid`
    fi
elif [ $1 = "stop" ]
then
    ps -p `cat server/server.pid` >& /dev/null
    if [ "$?" = "0" ]
    then
	echo "Killing local server with PID" `cat server/server.pid`
	kill `cat server/server.pid`
    else
	echo "Local server not running"
    fi
else
    echo "Invalid command specified $1"
    echo "Usage: $0 <start|stop>"
fi
